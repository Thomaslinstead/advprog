﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpGL;

namespace SharpGLWPFApplication1
{
    //Class inherits from Shape
    public class Chair_side : Shape
    {
        //Empty constructor
        public Chair_side() { }

        //Constructor with arguments
        public Chair_side(float BBMinX, float BBMinY, float BBMaxX, float BBMaxY) { 
            BBoxMin = new Point(BBMinX,BBMinY); 
            BBoxMax = new Point(BBMaxX,BBMaxY);
        }

        //Draw method
        public override int draw()
        {
            int selectionTotal = 0;
            Point t1, t2, m1, m2, m3, m4, b1, b2, b3, b4;

            //Define points
            t1 = new Point(BBoxMin.X, BBoxMin.Y);
            t2 = new Point(BBoxMin.X + ((BBoxMax.X - BBoxMin.X) / (float)8), BBoxMin.Y);
            m1 = new Point(BBoxMin.X + ((BBoxMax.X - BBoxMin.X) / (float)8), BBoxMax.Y + ((BBoxMin.Y - BBoxMax.Y)/(float)3));
            m2 = new Point(BBoxMax.X, m1.Y);
            m3 = new Point(m1.X, BBoxMax.Y + (((BBoxMin.Y - BBoxMax.Y) / (float)24) * (float)5));
            m4 = new Point(BBoxMax.X - ((BBoxMax.X - BBoxMin.X) / (float)8), m3.Y);
            b1 = new Point(BBoxMin.X, BBoxMax.Y);
            b2 = new Point(m3.X, BBoxMax.Y);
            b3 = new Point(m4.X, BBoxMax.Y);
            b4 = new Point(BBoxMax.X, BBoxMax.Y);

            //Draw lines
            selectionTotal += DrawMode.drawLine(LineType, t1, t2, colour);
            selectionTotal += DrawMode.drawLine(LineType, t2, m1, colour);
            selectionTotal += DrawMode.drawLine(LineType, m1, m2, colour);
            selectionTotal += DrawMode.drawLine(LineType, m2, b4, colour);
            selectionTotal += DrawMode.drawLine(LineType, b4, b3, colour);
            selectionTotal += DrawMode.drawLine(LineType, b3, m4, colour);
            selectionTotal += DrawMode.drawLine(LineType, m4, m3, colour);
            selectionTotal += DrawMode.drawLine(LineType, m3, b2, colour);
            selectionTotal += DrawMode.drawLine(LineType, b2, b1, colour);
            selectionTotal += DrawMode.drawLine(LineType, b1, t1, colour);
            return selectionTotal;
        }

        //Return type of shape
        public override string ToString()
        {
            return "Chair_side";
        }
    }
}
