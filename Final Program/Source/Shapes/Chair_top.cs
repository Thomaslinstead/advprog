﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpGL;

namespace SharpGLWPFApplication1
{
    //Class inherits from Shape
    public class Chair_top : Shape
    {
        //Empty constructor
        public Chair_top() { }

        //Constructor with arguments
        public Chair_top(float BBMinX, float BBMinY, float BBMaxX, float BBMaxY) { 
            BBoxMin = new Point(BBMinX,BBMinY); 
            BBoxMax = new Point(BBMaxX,BBMaxY);
        }

        //Draw method
        public override int draw()
        {
            int selectionTotal = 0;

            Point BLeftO;
            Point TLeftO;
            Point BRightO;
            Point TRightO;
            Point BLeftI;
            Point BRightI;
            Point TRightI;
            Point TLeftI;
            Point MLeft;
            Point MRight;

            //Define points
            if ((BBoxMin.X < BBoxMax.X && BBoxMin.Y < BBoxMax.Y) || (BBoxMin.X > BBoxMax.X && BBoxMin.Y > BBoxMax.Y))
            {
                BLeftO = new Point(BBoxMax.X, BBoxMin.Y);
                TLeftO = new Point(BBoxMin.X, BBoxMin.Y);
                BRightO = new Point(BBoxMax.X, BBoxMax.Y);
                TRightO = new Point(BBoxMin.X, BBoxMax.Y);
                BLeftI = new Point(BBoxMax.X, BBoxMin.Y + ((BBoxMax.Y - BBoxMin.Y) * (float)0.25));
                BRightI = new Point(BBoxMax.X, BBoxMin.Y + ((BBoxMax.Y - BBoxMin.Y) * (float)0.75));
                TRightI = new Point(BBoxMin.X + ((BBoxMax.X - BBoxMin.X) * (float)0.25), BRightI.Y);
                TLeftI = new Point(TRightI.X, BLeftI.Y);
                MLeft = new Point(BBoxMin.X + ((BBoxMax.X - BBoxMin.X) * (float)0.75), BLeftI.Y);
                MRight = new Point(MLeft.X, BRightI.Y);
            }
            else
            {
                BLeftO = new Point(BBoxMin.X, BBoxMax.Y);
                TLeftO = new Point(BBoxMin.X, BBoxMin.Y);
                BRightO = new Point(BBoxMax.X, BBoxMax.Y);
                TRightO = new Point(BBoxMax.X, BBoxMin.Y);
                BLeftI = new Point(BBoxMin.X + ((BBoxMax.X - BBoxMin.X) * (float)0.25), BBoxMax.Y);
                BRightI = new Point(BBoxMin.X + ((BBoxMax.X - BBoxMin.X) * (float)0.75), BBoxMax.Y);
                TRightI = new Point(BRightI.X, BBoxMin.Y + ((BBoxMax.Y - BBoxMin.Y) * (float)0.25));
                TLeftI = new Point(BLeftI.X, TRightI.Y);
                MLeft = new Point(BLeftI.X, BBoxMin.Y + ((BBoxMax.Y - BBoxMin.Y) * (float)0.75));
                MRight = new Point(BRightI.X, BBoxMin.Y + ((BBoxMax.Y - BBoxMin.Y) * (float)0.75));
            }

            //Draw lines
            selectionTotal += DrawMode.drawLine(LineType, BLeftO, TLeftO, colour);
            selectionTotal += DrawMode.drawLine(LineType, TLeftO, TRightO, colour);
            selectionTotal += DrawMode.drawLine(LineType, TRightO, BRightO, colour);
            selectionTotal += DrawMode.drawLine(LineType, BRightO, BRightI, colour);
            selectionTotal += DrawMode.drawLine(LineType, BRightI, TRightI, colour);
            selectionTotal += DrawMode.drawLine(LineType, TRightI, TLeftI, colour);
            selectionTotal += DrawMode.drawLine(LineType, TLeftI, BLeftI, colour);
            selectionTotal += DrawMode.drawLine(LineType, BLeftI, BLeftO, colour);
            selectionTotal += DrawMode.drawLine(LineType, MLeft, MRight, colour);
            return selectionTotal;
        }

        //Return type of shape
        public override string ToString()
        {
            return "Chair_top";
        }
    }
}
