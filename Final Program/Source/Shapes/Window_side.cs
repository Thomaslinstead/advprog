﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpGL;

namespace SharpGLWPFApplication1
{
    //Class inherits from Shape
    public class Window_side : Shape
    {
        //Empty constructor
        public Window_side() { }

        //Constructor with arguments
        public Window_side(float BBMinX, float BBMinY, float BBMaxX, float BBMaxY)
        {
            BBoxMin = new Point(BBMinX, BBMinY);
            BBoxMax = new Point(BBMaxX, BBMaxY);
        }

        //Draw method
        public override int draw()
        {
            int selectionTotal = 0;

            Point f1, f2, f3, f4, tl1, tl2, tl3, tl4, tr1, tr2, tr3, tr4, bl1, bl2, bl3, bl4, br1, br2, br3, br4;

            //Define points
            f1 = new Point(BBoxMin.X, BBoxMin.Y);
            f2 = new Point(BBoxMax.X, BBoxMin.Y);
            f3 = new Point(BBoxMin.X, BBoxMax.Y);
            f4 = new Point(BBoxMax.X, BBoxMax.Y);
            tl1 = new Point(BBoxMin.X + ((BBoxMax.X - BBoxMin.X) / (float)8), BBoxMin.Y - ((BBoxMin.Y - BBoxMax.Y) / (float)8));
            tl2 = new Point(BBoxMin.X + (((BBoxMax.X - BBoxMin.X) / (float)16) * (float)7), tl1.Y);
            tl3 = new Point(tl1.X, BBoxMax.Y + (((BBoxMin.Y - BBoxMax.Y) / (float)16) * (float)9));
            tl4 = new Point(tl2.X, tl3.Y);
            tr1 = new Point(tl2.X + ((BBoxMax.X - BBoxMin.X) / (float)8), tl2.Y);
            tr2 = new Point(BBoxMax.X - ((BBoxMax.X - BBoxMin.X) / (float)8), tr1.Y);
            tr3 = new Point(tr1.X, tl3.Y);
            tr4 = new Point(tr2.X, tr3.Y);
            bl1 = new Point(tl1.X, tl3.Y - ((BBoxMin.Y - BBoxMax.Y) / (float)8));
            bl2 = new Point(tl2.X, bl1.Y);
            bl3 = new Point(bl1.X, BBoxMax.Y + ((BBoxMin.Y - BBoxMax.Y) / (float)8));
            bl4 = new Point(bl2.X, bl3.Y);
            br1 = new Point(tr1.X, bl1.Y);
            br2 = new Point(tr2.X, br1.Y);
            br3 = new Point(br1.X, bl3.Y);
            br4 = new Point(br2.X, br3.Y);

            //Draw lines
            selectionTotal += DrawMode.drawLine(LineType, f1, f2, colour);
            selectionTotal += DrawMode.drawLine(LineType, f2, f4, colour);
            selectionTotal += DrawMode.drawLine(LineType, f4, f3, colour);
            selectionTotal += DrawMode.drawLine(LineType, f3, f1, colour);
            selectionTotal += DrawMode.drawLine(LineType, tl1, tl2, colour);
            selectionTotal += DrawMode.drawLine(LineType, tl2, tl4, colour);
            selectionTotal += DrawMode.drawLine(LineType, tl4, tl3, colour);
            selectionTotal += DrawMode.drawLine(LineType, tl3, tl1, colour);
            selectionTotal += DrawMode.drawLine(LineType, tr1, tr2, colour);
            selectionTotal += DrawMode.drawLine(LineType, tr2, tr4, colour);
            selectionTotal += DrawMode.drawLine(LineType, tr4, tr3, colour);
            selectionTotal += DrawMode.drawLine(LineType, tr3, tr1, colour);
            selectionTotal += DrawMode.drawLine(LineType, bl1, bl2, colour);
            selectionTotal += DrawMode.drawLine(LineType, bl2, bl4, colour);
            selectionTotal += DrawMode.drawLine(LineType, bl4, bl3, colour);
            selectionTotal += DrawMode.drawLine(LineType, bl3, bl1, colour);
            selectionTotal += DrawMode.drawLine(LineType, br1, br2, colour);
            selectionTotal += DrawMode.drawLine(LineType, br2, br4, colour);
            selectionTotal += DrawMode.drawLine(LineType, br4, br3, colour);
            selectionTotal += DrawMode.drawLine(LineType, br3, br1, colour);
            return selectionTotal;
        }

        //Return type of shape
        public override string ToString()
        {
            return "Window_side";
        }
    }
}