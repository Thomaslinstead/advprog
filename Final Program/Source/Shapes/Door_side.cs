﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpGL;

namespace SharpGLWPFApplication1
{
    //Class inherits from Shape
    public class Door_side : Shape
    {
        //Empty constructor
        public Door_side() { }

        //Constructor with arguments
        public Door_side(float BBMinX, float BBMinY, float BBMaxX, float BBMaxY) { 
            BBoxMin = new Point(BBMinX,BBMinY); 
            BBoxMax = new Point(BBMaxX,BBMaxY);
        }

        //Draw method
        public override int draw()
        {
            int selectionTotal = 0;

            Point f1, f2, f3, f4, lp1, lp2, lp3, lp4, rp1, rp2, rp3, rp4, bp1, bp2, bp3, bp4, h1, h2, h3, h4;

            //Define points
            f1 = new Point(BBoxMin.X, BBoxMin.Y);
            f2 = new Point(BBoxMax.X, BBoxMin.Y);
            f3 = new Point(BBoxMin.X, BBoxMax.Y);
            f4 = new Point(BBoxMax.X, BBoxMax.Y);
            lp1 = new Point(BBoxMin.X + ((BBoxMax.X - BBoxMin.X)/(float)8), BBoxMin.Y - ((BBoxMin.Y - BBoxMax.Y)/(float)8));
            lp2 = new Point(BBoxMin.X + (((BBoxMax.X - BBoxMin.X) / (float)16) * (float)7), lp1.Y);
            lp3 = new Point(lp1.X, BBoxMax.Y + ((BBoxMin.Y - BBoxMax.Y)/(float)2));
            lp4 = new Point(lp2.X, lp3.Y);
            rp1 = new Point(BBoxMax.X - (((BBoxMax.X - BBoxMin.X) / (float)16) * (float)7), lp1.Y);
            rp2 = new Point(BBoxMax.X - ((BBoxMax.X - BBoxMin.X) / (float)8), lp1.Y);
            rp3 = new Point(rp1.X, lp3.Y);
            rp4 = new Point(rp2.X, lp3.Y);
            bp1 = new Point(lp1.X, BBoxMax.Y + (((BBoxMin.Y - BBoxMax.Y) / (float)8) * (float)3));
            bp2 = new Point(rp2.X, bp1.Y);
            bp3 = new Point(lp1.X, BBoxMax.Y + ((BBoxMin.Y - BBoxMax.Y) / (float)8));
            bp4 = new Point(bp2.X, bp3.Y);
            h1 = new Point(BBoxMin.X + ((BBoxMax.X - BBoxMin.X) / (float)2), lp3.Y - ((BBoxMin.Y - BBoxMax.Y) / (float)32));
            h2 = new Point(rp2.X, h1.Y);
            h3 = new Point(h1.X, bp1.Y + ((BBoxMin.Y - BBoxMax.Y) / (float)32));
            h4 = new Point(h2.X, h3.Y);

            //Draw lines
            selectionTotal += DrawMode.drawLine(LineType, f1, f2, colour);
            selectionTotal += DrawMode.drawLine(LineType, f2, f4, colour);
            selectionTotal += DrawMode.drawLine(LineType, f4, f3, colour);
            selectionTotal += DrawMode.drawLine(LineType, f3, f1, colour);
            selectionTotal += DrawMode.drawLine(LineType, lp1, lp2, colour);
            selectionTotal += DrawMode.drawLine(LineType, lp2, lp4, colour);
            selectionTotal += DrawMode.drawLine(LineType, lp4, lp3, colour);
            selectionTotal += DrawMode.drawLine(LineType, lp3, lp1, colour);
            selectionTotal += DrawMode.drawLine(LineType, rp1, rp2, colour);
            selectionTotal += DrawMode.drawLine(LineType, rp2, rp4, colour);
            selectionTotal += DrawMode.drawLine(LineType, rp4, rp3, colour);
            selectionTotal += DrawMode.drawLine(LineType, rp3, rp1, colour);
            selectionTotal += DrawMode.drawLine(LineType, bp1, bp2, colour);
            selectionTotal += DrawMode.drawLine(LineType, bp2, bp4, colour);
            selectionTotal += DrawMode.drawLine(LineType, bp4, bp3, colour);
            selectionTotal += DrawMode.drawLine(LineType, bp3, bp1, colour);
            selectionTotal += DrawMode.drawLine(LineType, h1, h2, colour);
            selectionTotal += DrawMode.drawLine(LineType, h2, h4, colour);
            selectionTotal += DrawMode.drawLine(LineType, h4, h3, colour);
            selectionTotal += DrawMode.drawLine(LineType, h3, h1, colour);
            return selectionTotal;
        }

        //Return type of shape
        public override string ToString()
        {
            return "Door_side";
        }
    }
}
