﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpGL;

namespace SharpGLWPFApplication1
{
    //Class inherits from Shape
    public class Door_top : Shape
    {
        //Empty constructor
        public Door_top() { }

        //Constructor with arguments
        public Door_top(float BBMinX, float BBMinY, float BBMaxX, float BBMaxY)
        {
            BBoxMin = new Point(BBMinX, BBMinY);
            BBoxMax = new Point(BBMaxX, BBMaxY);
        }

        //Draw method
        public override int draw()
        {
            int selectionTotal = 0;

            Point o1, o2, o3, o4, i1, i2, i3, i4, m1, m2, ds, de;
            //Define points
            Colour whiteLine = new Colour(255, 255, 255);

            o1 = new Point(BBoxMin.X, BBoxMin.Y);
            o2 = new Point(BBoxMax.X, BBoxMax.Y);
            o3 = new Point(BBoxMin.X, BBoxMax.Y);
            o4 = new Point(BBoxMax.X, BBoxMin.Y);

            if (BBoxMin.X < BBoxMax.X && BBoxMin.Y > BBoxMax.Y)
            {
                i1 = new Point(BBoxMin.X + 2, BBoxMin.Y - 2);
                i2 = new Point(BBoxMax.X - 2, BBoxMin.Y - 2);
                i3 = new Point(BBoxMax.X - 2, BBoxMax.Y + 2);
                i4 = new Point(BBoxMin.X + 2, BBoxMax.Y + 2);
            }
            else if (BBoxMin.X > BBoxMax.X && BBoxMin.Y < BBoxMax.Y)
            {
                i1 = new Point(BBoxMax.X + 2, BBoxMax.Y - 2);
                i2 = new Point(BBoxMin.X - 2, BBoxMax.Y - 2);
                i3 = new Point(BBoxMin.X - 2, BBoxMin.Y + 2);
                i4 = new Point(BBoxMax.X + 2, BBoxMin.Y + 2);
            }
            else if (BBoxMin.X < BBoxMax.X && BBoxMin.Y < BBoxMax.Y)
            {
                i1 = new Point(BBoxMin.X + 2, BBoxMin.Y + 2);
                i2 = new Point(BBoxMax.X - 2, BBoxMin.Y + 2);
                i3 = new Point(BBoxMax.X - 2, BBoxMax.Y - 2);
                i4 = new Point(BBoxMin.X + 2, BBoxMax.Y - 2);
            }
            else
            {
                i1 = new Point(BBoxMax.X + 2, BBoxMin.Y - 2);
                i2 = new Point(BBoxMin.X - 2, BBoxMin.Y - 2);
                i3 = new Point(BBoxMin.X - 2, BBoxMax.Y + 2);
                i4 = new Point(BBoxMax.X + 2, BBoxMax.Y + 2);
            }

            if (Math.Abs(BBoxMax.Y - BBoxMin.Y) > Math.Abs(BBoxMax.X - BBoxMin.X))
            {
                ds = new Point(BBoxMin.X + ((BBoxMax.X - BBoxMin.X) / (float)2), BBoxMin.Y);
                de = new Point(BBoxMax.X + ((BBoxMax.X - BBoxMin.X) * (float)3), BBoxMax.Y);
                if (BBoxMin.Y < BBoxMax.Y)
                {
                    m1 = new Point(BBoxMin.X + ((BBoxMax.X - BBoxMin.X) / (float)2), BBoxMin.Y + 2);
                    m2 = new Point(BBoxMin.X + ((BBoxMax.X - BBoxMin.X) / (float)2), BBoxMax.Y - 2);
                }
                else
                {
                    m1 = new Point(BBoxMin.X + ((BBoxMax.X - BBoxMin.X) / (float)2), BBoxMin.Y - 2);
                    m2 = new Point(BBoxMin.X + ((BBoxMax.X - BBoxMin.X) / (float)2), BBoxMax.Y + 2);
                }

                //Draw lines
                selectionTotal += DrawMode.drawLine(LineType, o1, o3, whiteLine);
                selectionTotal += DrawMode.drawLine(LineType, o3, o2, colour);
                selectionTotal += DrawMode.drawLine(LineType, o2, o4, whiteLine);
                selectionTotal += DrawMode.drawLine(LineType, o4, o1, colour);
                selectionTotal += DrawMode.drawLine(LineType, i1, i2, whiteLine);
                selectionTotal += DrawMode.drawLine(LineType, i2, i3, whiteLine);
                selectionTotal += DrawMode.drawLine(LineType, i3, i4, whiteLine);
                selectionTotal += DrawMode.drawLine(LineType, i4, i1, whiteLine);
                selectionTotal += DrawMode.drawLine(LineType, m1, m2, whiteLine);
                selectionTotal += DrawMode.drawLine(LineType, ds, de, colour);
            }
            else
            {
                ds = new Point(BBoxMin.X, BBoxMin.Y + ((BBoxMax.Y - BBoxMin.Y) / (float)2));
                de = new Point(BBoxMax.X, BBoxMax.Y + ((BBoxMax.Y - BBoxMin.Y) * 3));
                if (BBoxMin.X < BBoxMax.X)
                {
                    m1 = new Point(BBoxMin.X + 2, BBoxMin.Y + ((BBoxMax.Y - BBoxMin.Y) / (float)2));
                    m2 = new Point(BBoxMax.X - 2, BBoxMin.Y + ((BBoxMax.Y - BBoxMin.Y) / (float)2));
                }
                else
                {
                    m1 = new Point(BBoxMin.X - 2, BBoxMin.Y + ((BBoxMax.Y - BBoxMin.Y) / (float)2));
                    m2 = new Point(BBoxMax.X + 2, BBoxMin.Y + ((BBoxMax.Y - BBoxMin.Y) / (float)2));
                }

                //Draw lines
                selectionTotal += DrawMode.drawLine(LineType, o1, o3, colour);
                selectionTotal += DrawMode.drawLine(LineType, o3, o2, whiteLine);
                selectionTotal += DrawMode.drawLine(LineType, o2, o4, colour);
                selectionTotal += DrawMode.drawLine(LineType, o4, o1, whiteLine);
                selectionTotal += DrawMode.drawLine(LineType, i1, i2, whiteLine);
                selectionTotal += DrawMode.drawLine(LineType, i2, i3, whiteLine);
                selectionTotal += DrawMode.drawLine(LineType, i3, i4, whiteLine);
                selectionTotal += DrawMode.drawLine(LineType, i4, i1, whiteLine);
                selectionTotal += DrawMode.drawLine(LineType, m1, m2, whiteLine);
                selectionTotal += DrawMode.drawLine(LineType, ds, de, colour);
            }
            return selectionTotal;
        }

        //Return type of shape
        public override string ToString()
        {
            return "Door_top";
        }
    }
}