﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpGL;

namespace SharpGLWPFApplication1
{
    //Class inherits from Shape
    public class InteriorWall : Shape
    {
        //Empty constructor
        public InteriorWall() { }

        //Constructor with arguments
        public InteriorWall(float BBMinX, float BBMinY, float BBMaxX, float BBMaxY)
        {
            BBoxMin = new Point(BBMinX, BBMinY);
            BBoxMax = new Point(BBMaxX, BBMaxY);
        }

        //Draw method
        public override int draw()
        {
            int selectionTotal = 0;

            Point o1, o2, o3, o4, i1, i2, i3, i4;

            //Define points
            o1 = new Point(BBoxMin.X, BBoxMin.Y);
            o2 = new Point(BBoxMax.X, BBoxMax.Y);
            o3 = new Point(BBoxMin.X, BBoxMax.Y);
            o4 = new Point(BBoxMax.X, BBoxMin.Y);

            if(BBoxMin.X < BBoxMax.X && BBoxMin.Y > BBoxMax.Y)
            {
                i1 = new Point(BBoxMin.X + 2, BBoxMin.Y - 2);
                i2 = new Point(BBoxMax.X - 2, BBoxMin.Y - 2);
                i3 = new Point(BBoxMax.X - 2, BBoxMax.Y + 2);
                i4 = new Point(BBoxMin.X + 2, BBoxMax.Y + 2);
            }
            else if(BBoxMin.X > BBoxMax.X && BBoxMin.Y < BBoxMax.Y)
            {
                i1 = new Point(BBoxMax.X + 2, BBoxMax.Y - 2);
                i2 = new Point(BBoxMin.X - 2, BBoxMax.Y - 2);
                i3 = new Point(BBoxMin.X - 2, BBoxMin.Y + 2);
                i4 = new Point(BBoxMax.X + 2, BBoxMin.Y + 2);
            }
            else if(BBoxMin.X < BBoxMax.X && BBoxMin.Y < BBoxMax.Y)
            {
                i1 = new Point(BBoxMin.X + 2, BBoxMin.Y + 2);
                i2 = new Point(BBoxMax.X - 2, BBoxMin.Y + 2);
                i3 = new Point(BBoxMax.X - 2, BBoxMax.Y - 2);
                i4 = new Point(BBoxMin.X + 2, BBoxMax.Y - 2);
            }
            else
            {
                i1 = new Point(BBoxMax.X + 2, BBoxMin.Y - 2);
                i2 = new Point(BBoxMin.X - 2, BBoxMin.Y - 2);
                i3 = new Point(BBoxMin.X - 2, BBoxMax.Y + 2);
                i4 = new Point(BBoxMax.X + 2, BBoxMax.Y + 2);
            }

            //Draw lines
            if (Math.Abs(BBoxMax.Y - BBoxMin.Y) > Math.Abs(BBoxMax.X - BBoxMin.X)) //Horizontal
            {
                selectionTotal += DrawMode.drawLine(LineType, o1, o3, colour);
                selectionTotal += DrawMode.drawLine(LineType, o2, o4, colour);
                selectionTotal += DrawMode.drawLine(LineType, i2, i3, colour);
                selectionTotal += DrawMode.drawLine(LineType, i4, i1, colour);

            }
            else if (BBoxMin.X < BBoxMax.X)
            {
                selectionTotal += DrawMode.drawLine(LineType, o3, o2, colour);
                selectionTotal += DrawMode.drawLine(LineType, o4, o1, colour);
                selectionTotal += DrawMode.drawLine(LineType, i1, i2, colour);
                selectionTotal += DrawMode.drawLine(LineType, i3, i4, colour);
            }
            return selectionTotal;
        }

        //Return type of shape
        public override string ToString()
        {
            return "InteriorWall";
        }
    }
}