﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpGL;

namespace SharpGLWPFApplication1
{
    //The Abstraction Shape Class
    public abstract class Shape
    {
        public ILine LineType { get; set; }
        public DrawingMode DrawMode { get; set; }

        //Properties with getter and setter
        public Colour colour { get; set; }
        public Point BBoxMin { get; set; }
        public Point BBoxMax { get; set; }

        public Shape DeepCopy()
        {
            Shape other = (Shape)this.MemberwiseClone();
            other.BBoxMin = new Point(this.BBoxMin.X, this.BBoxMin.Y);
            other.BBoxMax = new Point(this.BBoxMax.X, this.BBoxMax.Y);
            other.colour = new Colour(this.colour.R, this.colour.G, this.colour.B);
            return other;
        }

        //Overidable method draw.
        public abstract int draw();
    }
}
