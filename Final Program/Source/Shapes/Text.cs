﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpGL;
using PdfSharp.Drawing;
using PdfSharp.Pdf;

namespace SharpGLWPFApplication1
{
    //Class inherits from Shape
    class Text : Shape
    {
        //Class variables
        public string text { get; set; }

        //Empty constructor
        public Text() { }

        //Constructor with arguments
        public Text(float BBMinX, float BBMinY, float BBMaxX, float BBMaxY, string t) { 
            BBoxMin = new Point(BBMinX,BBMinY); 
            BBoxMax = new Point(BBMaxX,BBMaxY);
            text = t;
        }

        //Draw method
        public override int draw()
        {
            int selectionTotal = 0;

            float temp = 0;
            if (this.BBoxMax.X < this.BBoxMin.X)
            {
                temp = this.BBoxMax.X;
                this.BBoxMax.X = this.BBoxMin.X;
                this.BBoxMin.X = temp;
            }
            if (this.BBoxMax.Y < this.BBoxMin.Y)
            {
                temp = this.BBoxMax.Y;
                this.BBoxMax.Y = this.BBoxMin.Y;
                this.BBoxMin.Y = temp;
            }


            BBoxMax.X = BBoxMin.X + (text.Length * (Math.Abs(BBoxMax.Y - BBoxMin.Y)/5)*3);
            selectionTotal += DrawMode.drawText(BBoxMin, BBoxMax, colour, text, 0); //Draw Text

            //Draw Selection Box when selected
            if (colour.R == 256 && colour.G == 0 && colour.B == 0)
            drawBBox();

            return selectionTotal;
        }

        //Draw bounding box
        public void drawBBox()
        {
            //Define points
            Point XMin = new Point(BBoxMin.X, BBoxMax.Y);
            Point YMin = new Point(BBoxMax.X, BBoxMin.Y);
            //Draw lines
            LineType.drawLine(BBoxMin, XMin, colour);
            LineType.drawLine(XMin, BBoxMax, colour);
            LineType.drawLine(BBoxMax, YMin, colour);
            LineType.drawLine(YMin, BBoxMin, colour);
        }

        //Set text
        public void setText(string t){
            this.text = t;
        }

        //Return type of shape
        public override string ToString()
        {
            return "Text";
        }

    }
}
