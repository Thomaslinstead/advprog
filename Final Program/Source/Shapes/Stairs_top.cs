﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpGL;

namespace SharpGLWPFApplication1
{
    //Class inherits from Shape
    public class Stairs_top : Shape
    {
        //Empty constructor
        public Stairs_top() { }

        //Constructor with arguments
        public Stairs_top(float BBMinX, float BBMinY, float BBMaxX, float BBMaxY)
        {
            BBoxMin = new Point(BBMinX, BBMinY);
            BBoxMax = new Point(BBMaxX, BBMaxY);
        }

        //Draw method
        public override int draw()
        {
            int selectionTotal = 0;

            Point o1, o2, o3, o4, m1, m2;

            //Define points
            o1 = new Point(BBoxMin.X, BBoxMin.Y);
            o2 = new Point(BBoxMax.X, BBoxMax.Y);
            o3 = new Point(BBoxMin.X, BBoxMax.Y);
            o4 = new Point(BBoxMax.X, BBoxMin.Y);

            //Draw lines
            selectionTotal += DrawMode.drawLine(LineType, o1, o3, colour);
            selectionTotal += DrawMode.drawLine(LineType, o3, o2, colour);
            selectionTotal += DrawMode.drawLine(LineType, o2, o4, colour);
            selectionTotal += DrawMode.drawLine(LineType, o4, o1, colour);

            if (Math.Abs(BBoxMax.Y - BBoxMin.Y) > Math.Abs(BBoxMax.X - BBoxMin.X))
            {
                for (int i = 1; i < 9; i++)
                {
                    m1 = new Point(o1.X, o1.Y + (((o2.Y - o1.Y) / (float)8) * (float)i));
                    m2 = new Point(o2.X, o1.Y + (((o2.Y - o1.Y) / (float)8) * (float)i));
                    selectionTotal += DrawMode.drawLine(LineType, m1, m2, colour);
                }
            }
            else
            {
                for (int i = 1; i < 9; i++)
                {
                    m1 = new Point(o1.X + (((o2.X - o1.X) / (float)8) * (float)i), o1.Y);
                    m2 = new Point(o1.X + (((o2.X - o1.X) / (float)8) * (float)i), o2.Y);
                    selectionTotal += DrawMode.drawLine(LineType, m1, m2, colour);
                }
            }
            return selectionTotal;
        }

        //Return type of shape
        public override string ToString()
        {
            return "Stairs_top";
        }
    }
}