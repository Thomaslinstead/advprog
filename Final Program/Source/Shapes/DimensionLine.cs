﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpGL;

namespace SharpGLWPFApplication1
{
    //Class inherits from Shape
    class DimensionLine : Shape
    {
        //Empty constructor
        public DimensionLine() { }

        //Constructor with arguments
        public DimensionLine(float BBMinX, float BBMinY, float BBMaxX, float BBMaxY)
        { 
            BBoxMin = new Point(BBMinX,BBMinY); 
            BBoxMax = new Point(BBMaxX,BBMaxY);
        }

        //Draw method
        public override int draw()
        {
            int selectionTotal = 0;

            Point m1, m2;

            //Define points
            Point XMin = new Point(BBoxMin.X, BBoxMax.Y);
            Point YMin = new Point(BBoxMax.X, BBoxMin.Y);

            if (Math.Abs(BBoxMax.Y - BBoxMin.Y) > Math.Abs(BBoxMax.X - BBoxMin.X))
            {
                if (BBoxMin.Y < BBoxMax.Y)
                {
                    m1 = new Point(BBoxMin.X + ((BBoxMax.X - BBoxMin.X) / (float)2), BBoxMin.Y);
                    m2 = new Point(BBoxMin.X + ((BBoxMax.X - BBoxMin.X) / (float)2), BBoxMax.Y);
                }
                else
                {
                    m1 = new Point(BBoxMin.X + ((BBoxMax.X - BBoxMin.X) / (float)2), BBoxMin.Y);
                    m2 = new Point(BBoxMin.X + ((BBoxMax.X - BBoxMin.X) / (float)2), BBoxMax.Y);
                }
                //Draw lines (Horizontal)
                selectionTotal += DrawMode.drawLine(LineType, XMin, BBoxMax, colour); 
                selectionTotal += DrawMode.drawLine(LineType, YMin, BBoxMin, colour); 
            }
            else
            {
                if (BBoxMin.X < BBoxMax.X)
                {
                    m1 = new Point(BBoxMin.X, BBoxMin.Y + ((BBoxMax.Y - BBoxMin.Y) / (float)2));
                    m2 = new Point(BBoxMax.X, BBoxMin.Y + ((BBoxMax.Y - BBoxMin.Y) / (float)2));
                }
                else
                {
                    m1 = new Point(BBoxMin.X, BBoxMin.Y + ((BBoxMax.Y - BBoxMin.Y) / (float)2));
                    m2 = new Point(BBoxMax.X, BBoxMin.Y + ((BBoxMax.Y - BBoxMin.Y) / (float)2));
                }

                //Draw lines (Vertical)
                selectionTotal += DrawMode.drawLine(LineType, BBoxMin, XMin, colour); 
                selectionTotal += DrawMode.drawLine(LineType, BBoxMax, YMin, colour); 
            }

            //Define seperating points
            Point midPoint = new Point((BBoxMax.X + BBoxMin.X) / 2, (BBoxMax.Y + BBoxMin.Y) / 2);
            Point lowerPointmid = new Point((m1.X + midPoint.X) / 2, (m1.Y + midPoint.Y) / 2);
            Point lowerPoint = new Point((lowerPointmid.X + midPoint.X) / 2, (lowerPointmid.Y + midPoint.Y) / 2);
            Point upperPointmid = new Point((m2.X + midPoint.X) / 2, (m2.Y + midPoint.Y) / 2);
            Point upperPoint = new Point((upperPointmid.X + midPoint.X) / 2, (upperPointmid.Y + midPoint.Y) / 2);

            //Draw lines
            selectionTotal += DrawMode.drawLine(LineType, m1, lowerPoint, colour);
            selectionTotal += DrawMode.drawLine(LineType, upperPoint, m2, colour);

            //Calculate length
            double length = (double)((Math.Sqrt(((m2.X - m1.X) * (m2.X - m1.X)) + ((m2.Y - m1.Y) * (m2.Y - m1.Y)))) / (2.8571428571428571428571428571429 * 10)) * Grid.Instance.scale;

            //Create dimension size text
            String text = (string.Format("{0:0.00}", length)) + "cm";
            if (length >= 100 || length <= -100)
            {
                length = length / 100;
                text = (string.Format("{0:0.00}", length)) + "m";
            }
            
            //Point to shift text to the left of the middle
            Point leftmid = new Point(midPoint.X - 30, midPoint.Y);

            //Add dimensoon text to lines drawn
            selectionTotal += DrawMode.drawText(leftmid, midPoint, colour, text, 14);

            return selectionTotal;
        }

        //Return type of shape
        public override string ToString()
        {
            return "DimensionLine";
        }
    }
}
