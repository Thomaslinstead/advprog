﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpGL;

namespace SharpGLWPFApplication1
{
    //Class inherits from Shape
    public class Circle : Shape
    {
        //Empty constructor
        public Circle() { }

        //Constructor with arguments
        public Circle(float BBMinX, float BBMinY, float BBMaxX, float BBMaxY) { 
            BBoxMin = new Point(BBMinX,BBMinY); 
            BBoxMax = new Point(BBMaxX,BBMaxY);
        }

        //Draw method
        public override int draw()
        {
            int selectionTotal = 0;

            float cx = BBoxMin.X + (BBoxMax.X - BBoxMin.X)/2; //Center point in X
            float cy = BBoxMin.Y + (BBoxMax.Y - BBoxMin.Y) / 2; //Center point in Y
            int num_segments = 6 + (int)((cx + cy)/12); //Calculate number of segments required depending on size
            float r = (BBoxMax.X - cx); //Radius
            Point temp = new Point(0,0);
            Point current = new Point(0,0);
            for (int ii = 0; ii < num_segments+1; ii++) //Loop for whole circle
            {
                float theta = 2.0f * 3.1415926f * ii / num_segments;//get the current angle 
                float x = (float)(r * Math.Cos(theta));//calculate the x component 
                float y = (float)(r * Math.Sin(theta));//calculate the y component 

                current = new Point(x + cx, y + cy);//output vertex 
                if (ii > 0)
                {
                    selectionTotal += DrawMode.drawLine(LineType, current, temp, colour); //Draw line
                }
                temp = current;
            }
            return selectionTotal;
        }

        //Return type of shape
        public override string ToString()
        {
            return "Circle";
        }
    }
}
