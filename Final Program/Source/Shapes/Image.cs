﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpGL;
using System.Drawing;
using System.Drawing.Imaging;
using PdfSharp.Drawing;
using PdfSharp.Pdf;


namespace SharpGLWPFApplication1
{
    //Class inherits from Shape
    class Image : Shape
    {
        //Class variables
        public string path { get; set; }
        int textureID;

        //Empty constructor
        public Image() { }

        //Constructor with arguments
        public Image(float BBMinX, float BBMinY, float BBMaxX, float BBMaxY, int id, string p) { 
            BBoxMin = new Point(BBMinX,BBMinY); 
            BBoxMax = new Point(BBMaxX,BBMaxY);
            textureID = id;
            path = p;
        }

        //Draw method
        public override int draw()
        {
            int selectionTotal = 0;

            float temp = 0;
            if (this.BBoxMax.X < this.BBoxMin.X)
            {
                temp = this.BBoxMax.X;
                this.BBoxMax.X = this.BBoxMin.X;
                this.BBoxMin.X = temp;
            }
            if (this.BBoxMax.Y < this.BBoxMin.Y)
            {
                temp = this.BBoxMax.Y;
                this.BBoxMax.Y = this.BBoxMin.Y;
                this.BBoxMin.Y = temp;
            }

            //Draw image
            selectionTotal += DrawMode.drawImage(BBoxMin, BBoxMax, textureID, path);

            if(colour.R == 256 && colour.G == 0 && colour.B == 0) //Draw BBox when selected
            drawBBox();

            return selectionTotal;
        }

        //Draw bounding box
        public void drawBBox(){
            //Define points
            Point XMin = new Point(BBoxMin.X, BBoxMax.Y);
            Point YMin = new Point(BBoxMax.X, BBoxMin.Y);

            //Draw lines
            LineType.drawLine(BBoxMin, XMin, colour);
            LineType.drawLine(XMin, BBoxMax, colour);
            LineType.drawLine(BBoxMax, YMin, colour);
            LineType.drawLine(YMin, BBoxMin, colour);
        }

        //Return minimum value
        private float minValue(float a, float b)
        {
            if (a < b)
                return a;
            else
                return b;
        }

        //Return maximum value
        private float maxValue(float a, float b)
        {
            if (a > b)
                return a;
            else
                return b;
        }

        //Set ID
        public void setTextureID(int id){
            this.textureID = id;
        }

        //Set path
        public void setPath(string p)
        {
            this.path = p;
        }

        //Return type of shape
        public override string ToString()
        {
            return "Image";
        }
    }
}
