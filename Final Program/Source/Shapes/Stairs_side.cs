﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpGL;

namespace SharpGLWPFApplication1
{
    //Class inherits from Shape
    public class Stairs_side : Shape
    {
        //Empty constructor
        public Stairs_side() { }

        //Constructor with arguments
        public Stairs_side(float BBMinX, float BBMinY, float BBMaxX, float BBMaxY)
        { 
            BBoxMin = new Point(BBMinX,BBMinY); 
            BBoxMax = new Point(BBMaxX,BBMaxY);
        }

        //Draw method
        public override int draw()
        {
            int selectionTotal = 0;

            Point top, bottom, a, b, c;
            float width, height;

            //Define points
            if (BBoxMin.Y < BBoxMax.Y)
            {
                top = new Point(BBoxMax.X, BBoxMax.Y);
                bottom = new Point(BBoxMin.X, BBoxMin.Y);
            }
            else
            {
                top = new Point(BBoxMin.X, BBoxMin.Y);
                bottom = new Point(BBoxMax.X, BBoxMax.Y);
            }
            width = Math.Abs(top.X - bottom.X);
            height = Math.Abs(top.Y - bottom.Y);
            a = new Point(bottom.X, bottom.Y);
            for (int i = 1; i < 9; i++)
            {
                b = new Point(a.X, bottom.Y + (((top.Y - bottom.Y) / (float)8) * (float)i));
                c = new Point(bottom.X + (((top.X - bottom.X) / (float)8) * (float)i), bottom.Y + (((top.Y - bottom.Y) / (float)8) * (float)i));
                
                //Draw lines
                selectionTotal += DrawMode.drawLine(LineType, a, b, colour);
                selectionTotal += DrawMode.drawLine(LineType, b, c, colour);
                a = new Point(c.X, c.Y);
            }
            return selectionTotal;
        }

        //Return type of shape
        public override string ToString()
        {
            return "Stairs_side";
        }
    }
}
