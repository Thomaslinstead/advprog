﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpGLWPFApplication1
{
    //Class inherits from Shape
    public class Line : Shape
    {
        //Empty constructor
        public Line() { }

        //Constructor with arguments
        public Line(float BBMinX, float BBMinY, float BBMaxX, float BBMaxY) { 
            BBoxMin = new Point(BBMinX,BBMinY); 
            BBoxMax = new Point(BBMaxX,BBMaxY);
        }

        //Draw method
        public override int draw()
        {
            //Draw line
            return DrawMode.drawLine(LineType, BBoxMin, BBoxMax, colour);
        }

        //Return type of shape
        public override string ToString()
        {
            return "Line";
        }
    }
}