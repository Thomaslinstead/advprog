﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpGL;

namespace SharpGLWPFApplication1
{
    //Class inherits from Shape
    public class Square : Shape
    {
        //Empty constructor
        public Square() { }

        //Constructor with arguments
        public Square(float BBMinX, float BBMinY, float BBMaxX, float BBMaxY) { 
            BBoxMin = new Point(BBMinX,BBMinY); 
            BBoxMax = new Point(BBMaxX,BBMaxY);
        }

        //Draw method
        public override int draw()
        {
            int selectionTotal = 0;
            //Define points
            Point XMin = new Point(BBoxMin.X, BBoxMax.Y);
            Point YMin = new Point(BBoxMax.X, BBoxMin.Y);
            //Draw lines
            selectionTotal += DrawMode.drawLine(LineType, BBoxMin, XMin, colour);
            selectionTotal += DrawMode.drawLine(LineType, XMin, BBoxMax, colour);
            selectionTotal += DrawMode.drawLine(LineType, BBoxMax, YMin, colour);
            selectionTotal += DrawMode.drawLine(LineType, YMin, BBoxMin, colour);
            return selectionTotal;
        }

        //Return type of shape
        public override string ToString()
        {
            return "Square";
        }
    }
}
