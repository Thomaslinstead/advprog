﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpGLWPFApplication1
{
    //Class inherits from Shape
    class Curve : Shape
    {
        //Empty constructor
        public Curve() { }

        //Constructor with arguments
        public Curve(float BBMinX, float BBMinY, float BBMaxX, float BBMaxY) { 
            BBoxMin = new Point(BBMinX,BBMinY); 
            BBoxMax = new Point(BBMaxX,BBMaxY);
        }

        //Draw method
        public override int draw()
        {
            int selectionTotal = 0;

            Point POld = new Point(0,0);
            Point XMin = new Point(BBoxMin.X, BBoxMax.Y);
            Point YMin = new Point(BBoxMax.X, BBoxMin.Y);
            for (double t = 0.0; t <= 1.0; t += 0.1) //Increment t to advance the curve
            {

                Point P = drawBezier(BBoxMin, XMin, XMin, BBoxMax, t);
                if(t != 0)
                    selectionTotal += DrawMode.drawLine(LineType, POld, P, colour); //draw line
                POld = P;
            }
            return selectionTotal;
        }

        // Calculate the next bezier point.
        Point drawBezier(Point A, Point B, Point C, Point D, double t) {
            Point P = new Point(0,0);
            P.X = (float)(System.Math.Pow((1 - t), 3) * A.X + 3 * t * System.Math.Pow((1 - t), 2) * B.X + 3 * (1 - t) * System.Math.Pow(t, 2) * C.X + System.Math.Pow(t, 3) * D.X);
            P.Y = (float)(System.Math.Pow((1 - t), 3) * A.Y + 3 * t * System.Math.Pow((1 - t), 2) * B.Y + 3 * (1 - t) * System.Math.Pow(t, 2) * C.Y + System.Math.Pow(t, 3) * D.Y);
            return P;
        }

        //Return type of shape
        public override string ToString()
        {
            return "Curve";
        }
    }
}
