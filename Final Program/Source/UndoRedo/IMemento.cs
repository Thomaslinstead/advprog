﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpGLWPFApplication1
{
    /* Generic Interface for Memento state classes to implement */
    public interface IMemento<T>
    {
        IMemento<T> Restore(T target);
    }
}
