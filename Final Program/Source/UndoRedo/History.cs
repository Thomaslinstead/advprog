﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpGLWPFApplication1
{
    /* Singleton class for handling the undo/redo history list */ 
    class History
    {
        private static History instance;
        public UndoRedo<ShapeList> history;

        /* Default Constructor */
        private History()
        { 
            
        }

        /* Create new history list */
        public void createList(ShapeList s){
            history = new UndoRedo<ShapeList>(s, 2);
        }

        /* Singleton Instance */
        public static History Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new History();
                }
                return instance;
            }
        }


    }
}
