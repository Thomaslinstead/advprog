﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpGLWPFApplication1
{
        /* Memento of type ShapeList */
        abstract class ShapeMemento : IMemento<ShapeList>
        {
            public abstract IMemento<ShapeList> Restore(ShapeList target);
        }

        /* Memento state class for adding a shape action */
        class AddShapeMemento : ShapeMemento
        {
            public AddShapeMemento()
            {
            }

            /* Method to call to restore previous state */
            public override IMemento<ShapeList> Restore(ShapeList target)
            {
                IMemento<ShapeList> opposite = new RemoveShapeMemento(target.getLastShape());
                target.removeLast();
                return opposite;
            }
        }

        /* Memento state class for removing a shape action */
        class RemoveShapeMemento : ShapeMemento
        {
            Shape removed;
            public RemoveShapeMemento(Shape removed)
            {
                this.removed = removed;
            }

            /* Method to call to restore previous state */
            public override IMemento<ShapeList> Restore(ShapeList target)
            {
                IMemento<ShapeList> opposite = new AddShapeMemento();
                target.addShape(removed);
                return opposite;
            }
        }

        /* Memento state class for resizing a shape action */
        class MoveShapeMemento : ShapeMemento
        {
            Shape moved; 
            float moveX, moveY;
            public MoveShapeMemento(Shape moved, float moveX, float moveY)
            {
                this.moved = moved;
                this.moveX = moveX;
                this.moveY = moveY;
            }

            /* Method to call to restore previous state */
            public override IMemento<ShapeList> Restore(ShapeList target)
            {
                moved.BBoxMax.X = moved.BBoxMax.X - moveX;
                moved.BBoxMax.Y = moved.BBoxMax.Y - moveY;
                moved.BBoxMin.X = moved.BBoxMin.X - moveX;
                moved.BBoxMin.Y = moved.BBoxMin.Y - moveY;
                IMemento<ShapeList> opposite = new MoveShapeMemento(moved, moveX*-1, moveY*-1);
                return opposite;
            }
        }

        /* Memento state class for adding a shape action*/
        class ResizeShapeMemento : ShapeMemento
        {
            Shape resized;
            float xOffset, yOffset;
            bool position;
            public ResizeShapeMemento(Shape s, float x, float y, bool t)
            {
                this.resized = s;
                this.xOffset = x;
                this.yOffset = y;
                this.position = t;
            }

            /* Method to call to restore previous state */
            public override IMemento<ShapeList> Restore(ShapeList target)
            {
                if (position == true)
                {
                    resized.BBoxMax.X -= xOffset;
                    resized.BBoxMax.Y -= yOffset;
                }
                else
                {
                    resized.BBoxMin.X -= xOffset;
                    resized.BBoxMin.Y -= yOffset;
                }
                IMemento<ShapeList> opposite = new ResizeShapeMemento(resized, xOffset * -1, yOffset*-1,position);
                return opposite;
            }
        }
}
