﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpGLWPFApplication1
{
    /* Class for handling a generic undo/redo list */
    class UndoRedo<T>
    {
        protected T subject; //Generic type

        //Stack data structure for holding the lists.
        private Stack<IMemento<T>> undoList = new Stack<IMemento<T>>();
        private Stack<IMemento<T>> redoList = new Stack<IMemento<T>>();

        /* Constructor for Undo/Redo object *
         *   subject - generic type of list *
         *   capacity - maximum list size   */
        public UndoRedo(T subject, int capacity)
        {
            this.subject = subject;
            undoList = new Stack<IMemento<T>>(capacity);
            redoList = new Stack<IMemento<T>>(capacity);
        }

        /* Add Memento state change action to list */ 
        public void action(IMemento<T> m)
        {
            redoList.Clear();
            undoList.Push(m);
        }

        /* Restore the previous state */
        public void undo()
        {
            IMemento<T> top = undoList.Pop();
            redoList.Push(top.Restore(subject));
        }

        /* Restore the previous undo state */
        public void redo()
        {
            IMemento<T> top = redoList.Pop();
            undoList.Push(top.Restore(subject));
        }

        /* Check to see if its possible to undo */
        public bool CanUndo
        {
            get { return (undoList.Count != 0); } //is list empty?
        }

        /* Check to see if its possible to redo */
        public bool CanRedo
        {
            get { return (redoList.Count != 0); } //is list empty?
        }
    }
}
