﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpGLWPFApplication1
{
    public sealed class SaveWork
    {
        //Class Variables
        private static SaveWork instance;
        private System.IO.StreamWriter saveFile;
        private string savePath = ""; 

        //Empty constructor
        private SaveWork()
        {
        }

        //Get instance of class
        public static SaveWork Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SaveWork();
                }
                return instance;
            }
        }

        //Create a file containing shape data of shapes in list
        public void save(String filename, ShapeList list)
        {
            //Create file
            saveFile = new System.IO.StreamWriter(filename);

            int size = list.size();
            Shape temp;

            //Write first line to contain page setup information
            saveFile.WriteLine(Grid.Instance.pageLimitX + "," + 
                               Grid.Instance.pageLimitY + "," + 
                               Grid.Instance.panX + "," + 
                               Grid.Instance.panY + "," +
                               PDFExport.Instance.getPageSize() + "," +
                               PDFExport.Instance.getPageOrientation());
 
            //Write data for each shape on separate lines, separating data with commas
            for (int i = 0; i < size; i++)
            {
                temp = list.getShape(i);
                String param = "";
                if (temp.ToString().Equals("Image")){
                    Image t = (Image)temp;
                    param = t.path;
                } else if(temp.ToString().Equals("Text"))
                {
                    Text t = (Text)temp;
                    param = t.text.Replace(",", "&#130;"); //Stop commas breaking the file
                }
                else
                {
                    param = temp.LineType.ToString();
                }
                saveFile.WriteLine(temp.ToString() + ","
                    + param + ","
                    + temp.colour.R + "," + temp.colour.G + "," + temp.colour.B + "," + temp.colour.A + ","
                    + temp.BBoxMin.X + "," + temp.BBoxMin.Y + ","
                    + temp.BBoxMax.X + "," + temp.BBoxMax.Y);
            }

            //Close file
            saveFile.Close();
        }

        public string saveClick(ShapeList list, String filename)
        {
            // Configure save file dialog box
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = filename; // Default file name
            dlg.DefaultExt = ".2dc"; // Default file extension
            dlg.Filter = "2dCAD documents (.2dc)|*.2dc"; // Filter files by extension
            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results 
            if (result == true)
            {
                // Save document 
                string fileName = dlg.FileName;
                save(fileName, list);
                LoadWork.Instance.filePath = fileName;
            }

            return dlg.SafeFileName;
        }            
    }
}
