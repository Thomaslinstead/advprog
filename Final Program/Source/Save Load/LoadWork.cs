﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;

namespace SharpGLWPFApplication1
{
    public sealed class LoadWork
    {
        //Class Variables
        private static LoadWork instance;
        public string fileName;
        public string filePath { get; set; }
        private System.IO.StreamReader loadFile;

        //Empty constructor
        private LoadWork()
        {
        }

        //Get instance of class
        public static LoadWork Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new LoadWork();
                }
                return instance;
            }
        }

        //Load shape data from a file
        public void load(String filename, ShapeList list)
        {
            //Open file
            loadFile = new System.IO.StreamReader(filename);

            //Clear shape list
            list.clearList();

            String nextLine;
            String[] lineSplit;
            Shape temp;
            ILine tempILine;
            Colour tempColour;
            Point tempBBoxMin, tempBBoxMax;
            bool firstLine = true;
            try
            {
                //Use regular expression to extract shape information from comma delimited file
                while (!loadFile.EndOfStream)
                {
                    nextLine = loadFile.ReadLine();
                    lineSplit = Regex.Split(nextLine, ",");

                    //Get page setup information from first line of file
                    if (firstLine == true)
                    {
                        Grid.Instance.pageLimitX = int.Parse(lineSplit[0]);
                        Grid.Instance.pageLimitY = int.Parse(lineSplit[1]);
                        Grid.Instance.panX = float.Parse(lineSplit[2]);
                        Grid.Instance.panY = float.Parse(lineSplit[3]);
                        PDFExport.Instance.setPageSize(int.Parse(lineSplit[4]));
                        PDFExport.Instance.setPageOrientation(bool.Parse(lineSplit[5]));
                        firstLine = false;
                    }
                    else
                    {
                        String additionalInfo = "";

                        if (lineSplit[1].Equals("SolidLine"))
                            tempILine = new SolidLine();
                        else if (lineSplit[1].Equals("DottedLine"))
                            tempILine = new DottedLine();
                        else if (lineSplit[1].Equals("DashedLine"))
                            tempILine = new DashedLine();
                        else
                        {
                            tempILine = new SolidLine();
                            additionalInfo = lineSplit[1];
                        }

                        //Get bounding box points from data
                        tempBBoxMin = new Point(float.Parse(lineSplit[6]), float.Parse(lineSplit[7]));
                        tempBBoxMax = new Point(float.Parse(lineSplit[8]), float.Parse(lineSplit[9]));

                        //Create appropriate shape
                        temp = new Line();
                        if (lineSplit[0].Equals("Chair_side"))
                            temp = new Chair_side();
                        else if (lineSplit[0].Equals("Chair_top"))
                            temp = new Chair_top();
                        else if (lineSplit[0].Equals("Circle"))
                            temp = new Circle();
                        else if (lineSplit[0].Equals("Curve"))
                            temp = new Curve();
                        else if (lineSplit[0].Equals("DimensionLine"))
                            temp = new DimensionLine();
                        else if (lineSplit[0].Equals("Door_side"))
                            temp = new Door_side();
                        else if (lineSplit[0].Equals("Door_top"))
                            temp = new Door_top();
                        else if (lineSplit[0].Equals("ExteriorWall"))
                            temp = new ExteriorWall();
                        else if (lineSplit[0].Equals("InteriorWall"))
                            temp = new InteriorWall();
                        else if (lineSplit[0].Equals("Line"))
                            temp = new Line();
                        else if (lineSplit[0].Equals("Square"))
                            temp = new Square();
                        else if (lineSplit[0].Equals("Stairs_side"))
                            temp = new Stairs_side();
                        else if (lineSplit[0].Equals("Stairs_top"))
                            temp = new Stairs_top();
                        else if (lineSplit[0].Equals("Window_side"))
                            temp = new Window_side();
                        else if (lineSplit[0].Equals("Window_top"))
                            temp = new Window_top();
                        else if (lineSplit[0].Equals("Image"))
                        {
                            Textures.Instance.initializeTexture(additionalInfo);
                            Image t = new Image();
                            t.setTextureID(Textures.Instance.textureCount() - 1);
                            t.setPath(additionalInfo);
                            temp = t;
                        }
                        else if (lineSplit[0].Equals("Text"))
                        {
                            Text t = new Text();
                            t.setText(additionalInfo.Replace("&#130;", ","));
                            temp = t;
                        }

                        //Assign Properties
                        tempColour = new Colour(float.Parse(lineSplit[2]),
                            float.Parse(lineSplit[3]), float.Parse(lineSplit[4]));
                        temp.LineType = tempILine;
                        temp.DrawMode = new OpenGLDraw();
                        temp.colour = tempColour;
                        temp.BBoxMin = tempBBoxMin;
                        temp.BBoxMax = tempBBoxMax;

                        //Add shape to shape list
                        list.addShape(temp);
                    }
                }

                //Close file
                loadFile.Close();
            }
            catch (Exception E)
            {
                list.clearList();
                String error = "Invalid file format or the file is corrupt at path:\n " + filePath + "\n (File format: .2dc etc)  \n\nDebug Message: \n" + E.ToString(); //Get the error

                // Configure error message box
                string caption = "Error while loading file:";
                MessageBoxButton button = MessageBoxButton.OK;
                MessageBoxImage icon = MessageBoxImage.Error;

                // Display message box
                MessageBoxResult messageBoxResult = MessageBox.Show(error, caption, button, icon);
            }
        }

        public void loadClick(ShapeList list)
        {
            // Configure open file dialog box
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = "Document"; // Default file name
            fileName = dlg.SafeFileName;//FileName;
            dlg.DefaultExt = ".2dc"; // Default file extension
            dlg.Filter = "2dCAD documents (.2dc)|*.2dc"; // Filter files by extension 

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results 
            if (result == true)
            {
                // Open document 
                filePath = dlg.FileName;
                fileName = dlg.SafeFileName;
                load(dlg.FileName, list);
            }
        }

        public String loadFilePath()
        {
            // Configure open file dialog box
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = "File"; // Default file name
            dlg.InitialDirectory = System.IO.Directory.GetCurrentDirectory();
            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results 
            if (result == true)
            {
                // Open document 
                return dlg.FileName;
            }
            return null;
        }
    }
}
