﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpGLWPFApplication1
{
    /* Point Object class for storing coordinate values */
    public class Point
    {
        //Construct a point with two values 
        public Point(float x1, float y1) {
            X = x1;
            Y = y1;
        }

        //Getter and setter methods
        public float X { get; set; }
        public float Y { get; set; }

        //Method to set the values
        public void setLocation(float XPos, float YPos) {
            X = XPos;
            Y = YPos;
        }
    }
}
