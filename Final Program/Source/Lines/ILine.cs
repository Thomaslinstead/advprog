﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpGLWPFApplication1
{
    /* Interface for line style bridge implementor */ 
    public interface ILine
    {
        void drawLine(Point pointA, Point pointB, Colour colour);
        void PDFDrawLine(Point pointA, Point pointB, Colour colour);
    }
}
