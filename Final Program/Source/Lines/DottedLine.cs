﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpGL;
using PdfSharp.Drawing;

namespace SharpGLWPFApplication1
{
    /* Concrete Implementor class for drawing dotted lines */
    public class DottedLine : ILine
    {
        /* Draw Dotted Line using OpenGL */
        public void drawLine(Point pointA, Point pointB, Colour colour)
        {
            OpenGLHandler glInstance = OpenGLHandler.Instance;
            OpenGL gl = glInstance.getOpenGLReference();
            gl.PushMatrix();
            gl.PushAttrib(OpenGL.GL_ENABLE_BIT);
            gl.LineStipple(1, 0xAAAA);  //Set to dotted line
            gl.Enable(OpenGL.GL_LINE_STIPPLE);
            gl.Begin(OpenGL.GL_LINES);
            gl.Color(colour.R, colour.G, colour.B);
            gl.Vertex(pointA.X, pointA.Y);
            gl.Vertex(pointB.X, pointB.Y);
            gl.End();
            gl.PopAttrib();
            gl.PopMatrix();
        }

        /* Draw Dotted Line using PDFSharp */
        public void PDFDrawLine(Point pointA, Point pointB, Colour colour)
        {
            PDFExport pdfInstance = PDFExport.Instance;
            XPen pen = new XPen(XColor.FromArgb((byte)colour.R, (byte)colour.G, (byte)colour.B), 1);
            pen.DashStyle = XDashStyle.Dot;
            pdfInstance.gfx.DrawLine(pen, pointA.X, pointA.Y, pointB.X, pointB.Y);
        }

        /* Return the class as a a string */
        public override string ToString()
        {
            return "DottedLine";
        }
    }
}
