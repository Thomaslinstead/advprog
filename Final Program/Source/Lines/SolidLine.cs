﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpGL;
using PdfSharp.Drawing;

namespace SharpGLWPFApplication1
{
    /* Concrete Implementor class for drawing solid lines */
    public class SolidLine : ILine
    {
        /* Draw Solid Line using OpenGL */
        public void drawLine(Point pointA, Point pointB, Colour colour)
        {
            OpenGLHandler glInstance = OpenGLHandler.Instance;
            OpenGL gl = glInstance.getOpenGLReference();
            gl.PushMatrix();
            gl.Begin(OpenGL.GL_LINES);
            gl.Color(colour.R, colour.G, colour.B);
            gl.Vertex(pointA.X, pointA.Y);
            gl.Vertex(pointB.X, pointB.Y);
            gl.End();
            gl.PopMatrix();
        }

        /* Draw Solid Line using PDFSharp */
        public void PDFDrawLine(Point pointA, Point pointB, Colour colour)
        {
            PDFExport pdfInstance = PDFExport.Instance;
            XPen pen = new XPen(XColor.FromArgb((byte)colour.R, (byte)colour.G, (byte)colour.B), 1);
            pen.DashStyle = XDashStyle.Solid;
            pdfInstance.gfx.DrawLine(pen, pointA.X, pointA.Y, pointB.X, pointB.Y);
        }

        /* Return the class as a a string */
        public override string ToString()
        {
            return "SolidLine";
        }
    }
}
