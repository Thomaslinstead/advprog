﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpGLWPFApplication1
{
    /* Colour Object class for storing colour values */
    public class Colour
    {
        /* Constructor without alpha */
        public Colour(float r, float g, float b) {
            R = r;
            G = g;
            B = b;
            A = 1;
        }

        /* Constructor with alpha */
        public Colour(float r, float g, float b, float a)
        {
            R = r;
            G = g;
            B = b;
            A = a;
        }

        //Getter and setter methods
        public float R { get; set; }
        public float G { get; set; }
        public float B { get; set; }
        public float A { get; set; }

        /* Set the current RGB values */
        public void setColour(float r, float g, float b) {
            R = r;
            G = g;
            B = b;
        }
    }
}
