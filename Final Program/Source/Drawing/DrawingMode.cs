﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpGLWPFApplication1
{
    /* Interface for drawing style bridge implementor */ 
    public interface DrawingMode
    {
        int drawLine(ILine LineType, Point pointA, Point pointB, Colour colour);
        int drawText(Point pointA, Point pointB, Colour colour, string t, int height);
        int drawImage(Point pointA, Point pointB, int id, string p);
    }
}
