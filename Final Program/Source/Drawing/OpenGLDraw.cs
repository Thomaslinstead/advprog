﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpGL;

namespace SharpGLWPFApplication1
{
    /* Concrete Implementor class for drawing with OpenGL */
    public class OpenGLDraw : DrawingMode
    {
        /* Draw Line to OpenGL Display */
        public int drawLine(ILine LineType, Point BBoxMin, Point BBoxMax, Colour colour)
        {
            LineType.drawLine(BBoxMin, BBoxMax, colour);
            return 0; //Success
        }

        /* Draw Text to OpenGL Display */
        public int drawText(Point BBoxMin, Point BBoxMax, Colour colour, string t, int height)
        {
            OpenGLHandler glInstance = OpenGLHandler.Instance;
            OpenGL gl = glInstance.getOpenGLReference();
            String faceName = "Arial";
            gl.PushMatrix();
            if(height == 0) //Calculate Height based on BBox
                gl.DrawText((int)((BBoxMin.X - Grid.Instance.panX) * Grid.Instance.zoom), (int)((glInstance.Height - ((BBoxMax.Y - Grid.Instance.panY) * Grid.Instance.zoom))), colour.R, colour.G, colour.B, faceName, Math.Abs((int)((BBoxMax.Y - BBoxMin.Y) * Grid.Instance.zoom)), t);
            else  //Draw using specified Height
                gl.DrawText((int)((BBoxMin.X - Grid.Instance.panX) * Grid.Instance.zoom), (int)((glInstance.Height - ((BBoxMax.Y - Grid.Instance.panY) * Grid.Instance.zoom))), colour.R, colour.G, colour.B, faceName, Math.Abs((int)((height) * Grid.Instance.zoom)), t);
            gl.PopMatrix();
            return 0; //Success
        }

        /* Draw Image to OpenGL Display */
        public int drawImage(Point BBoxMin, Point BBoxMax, int id, string p)
        {
            OpenGLHandler glInstance = OpenGLHandler.Instance;
            OpenGL gl = glInstance.getOpenGLReference();
            gl.Color(1.0f, 1.0f, 1.0f);
            gl.Enable(OpenGL.GL_TEXTURE_2D);
            gl.BindTexture(OpenGL.GL_TEXTURE_2D, Textures.Instance.textures[id][0]); //Bind Texture
            gl.PushMatrix();
            gl.PushAttrib(OpenGL.GL_ENABLE_BIT);
            gl.Begin(OpenGL.GL_QUADS); //Draw square quad and texture
            Point XMin = new Point(BBoxMin.X, BBoxMax.Y);
            Point YMin = new Point(BBoxMax.X, BBoxMin.Y);
            gl.TexCoord(0.0f, 0.0f); gl.Vertex(BBoxMin.X, BBoxMin.Y);	// Bottom Left Of The Texture and Quad
            gl.TexCoord(1.0f, 0.0f); gl.Vertex(YMin.X, YMin.Y);	// Bottom Right Of The Texture and Quad
            gl.TexCoord(1.0f, 1.0f); gl.Vertex(BBoxMax.X, BBoxMax.Y);	// Top Right Of The Texture and Quad
            gl.TexCoord(0.0f, 1.0f); gl.Vertex(XMin.X, XMin.Y);	// Top Left Of The Texture and Quad
            gl.End();
            gl.PopAttrib();
            gl.PopMatrix();
            gl.Disable(OpenGL.GL_TEXTURE_2D);
            gl.Color(0.0f, 0.0f, 0.0f);
            return 0; //Success
        }
    }
}
