﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System.Windows;

namespace SharpGLWPFApplication1
{
    /* Concrete Implementor class for drawing with PDFSharp */
    public class PDFDraw : DrawingMode
    {
        /* Draw Line to PDF */
        public int drawLine(ILine LineType, Point BBoxMin, Point BBoxMax, Colour colour)
        {
            LineType.PDFDrawLine(BBoxMin, BBoxMax, colour);
            return 0; //Success
        }

        /* Draw Text to PDF */
        public int drawText(Point BBoxMin, Point BBoxMax, Colour colour, string t, int height)
        {
            PDFExport pdfInstance = PDFExport.Instance;
            XFont font;
            if (height == 0) //Calculate Height based on BBox
                font = new XFont("Arial", Math.Abs((int)(BBoxMax.Y - BBoxMin.Y)) + 2, XFontStyle.Regular);
            else  //Draw using specified Height
                font = new XFont("Arial", Math.Abs((int)(height)) + 2, XFontStyle.Regular);
            
            pdfInstance.gfx.DrawString(t, font, new XSolidBrush(XColor.FromArgb((byte)colour.R, (byte)colour.G, (byte)colour.B)), (float)BBoxMin.X, (float)BBoxMax.Y);
            return 0; //Success
        }

        /* Draw Image to PDF */
        public int drawImage(Point BBoxMin, Point BBoxMax, int id, string p)
        {
            try
            {
                PDFExport pdfInstance = PDFExport.Instance;
                XImage image = XImage.FromFile(p); //Load image from file
                pdfInstance.gfx.DrawImage(image, minValue(BBoxMin.X, BBoxMax.X), minValue(BBoxMin.Y, BBoxMax.Y),
                    maxValue(BBoxMin.X, BBoxMax.X) - minValue(BBoxMin.X, BBoxMax.X),
                    maxValue(BBoxMin.Y, BBoxMax.Y) - minValue(BBoxMin.Y, BBoxMax.Y));
                return 0; //Success
            }
            catch (Exception E)
            {
                MessageBox.Show("Error occured while loading Image for PDF Export.", E.ToString(), MessageBoxButton.OK, MessageBoxImage.Warning);
                return -1; //Return Failure
            }
        }

        /* find the minimum value of two floats a & b */
        private float minValue(float a, float b)
        {
            if (a < b)
                return a;
            else
                return b;
        }

        /* find the maximum value of two floats a & b */
        private float maxValue(float a, float b)
        {
            if (a > b)
                return a;
            else
                return b;
        }

    }
}
