﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpGLWPFApplication1
{
    /* Concrete Implementor class for getting the drawing selection */
    public class SelectionDraw : DrawingMode
    {
        /* Select line */
        public int drawLine(ILine LineType, Point BBoxMin, Point BBoxMax, Colour colour)
        {
            return (Select.Instance.selectLine(BBoxMin, BBoxMax));
        }

        /* Select Text */
        public int drawText(Point BBoxMin, Point BBoxMax, Colour colour, string t, int height)
        {
            BBoxMax.X = BBoxMin.X + (t.Length * (Math.Abs(BBoxMax.Y - BBoxMin.Y) / 5) * 3);
            return Select.Instance.selectBBox(BBoxMin, BBoxMax);
        }

        /* Select Image */
        public int drawImage(Point BBoxMin, Point BBoxMax, int id, string p)
        {
            return Select.Instance.selectBBox(BBoxMin, BBoxMax);
        }
    }
}
