﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SharpGLWPFApplication1.Windows
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    public partial class About : Window
    {
        //Constructor.
        public About()
        {
            InitializeComponent();
        }

        //Close window on OK click (logic).
        private void OK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
