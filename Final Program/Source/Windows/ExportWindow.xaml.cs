﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Drawing;
using System.Drawing.Imaging;
using SharpGL;

namespace SharpGLWPFApplication1.Windows
{
    /// <summary>
    /// Interaction logic for ExportWindow.xaml
    /// </summary>
    public partial class ExportWindow : Window
    {
        ShapeList elist;
        OpenGL ogl;

        //Constructor.
        public ExportWindow(ShapeList list, OpenGL gl)
        {
            InitializeComponent();
            elist = list;
            ogl = gl;
        }

        //Cancel button logic.
        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false; //Close Window.
        }

        //OK button logic.
        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            //Export to PDF.
            if (PDFExportComboBoxItem.IsSelected)
            {
                PDFExport.Instance.Export(elist);
            }
            //Export to Bitmap.
            else if (BMPExportComboBoxItem.IsSelected)
            {
                // Configure save file dialog box
                Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                dlg.FileName = "bmpoutput"; // Default file name
                dlg.DefaultExt = ".bmp"; // Default file extension
                dlg.Filter = "bmp documents (.bmp)|*.bmp"; // Filter files by extension

                // Show save file dialog box
                Nullable<bool> result = dlg.ShowDialog();

                if (result == true)
                {
                    //Initialize RenderContext as DIB section.
                    SharpGL.RenderContextProviders.DIBSectionRenderContextProvider provider = (SharpGL.RenderContextProviders.DIBSectionRenderContextProvider)ogl.RenderContextProvider;

                    //Create drawing from GDI Bitmap.
                    Bitmap Image = Bitmap.FromHbitmap(provider.DIBSection.HBitmap);

                    //Save image.
                    Image.Save(dlg.FileName, ImageFormat.Bmp);

                    //Display image.
                    Process.Start(dlg.FileName);
                }
            }
            //Export to GIF.
            else if (GIFExportComboBoxItem.IsSelected)
            {
                // Configure save file dialog box
                Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                dlg.FileName = "gifoutput"; // Default file name
                dlg.DefaultExt = ".gif"; // Default file extension
                dlg.Filter = "gif documents (.gif)|*.gif"; // Filter files by extension

                // Show save file dialog box
                Nullable<bool> result = dlg.ShowDialog();

                if (result == true)
                {
                    //Initialize RenderContext as DIB section.
                    SharpGL.RenderContextProviders.DIBSectionRenderContextProvider provider = (SharpGL.RenderContextProviders.DIBSectionRenderContextProvider)ogl.RenderContextProvider;

                    //Create drawing from GDI Bitmap.
                    Bitmap Image = Bitmap.FromHbitmap(provider.DIBSection.HBitmap);

                    //Save image.
                    Image.Save(dlg.FileName, ImageFormat.Gif);

                    //Display image.
                    Process.Start(dlg.FileName);
                }
            }
            //Export to JPEG.
            else if (JPGExportComboBoxItem.IsSelected)
            {
                // Configure save file dialog box
                Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                dlg.FileName = "jpgoutput"; // Default file name
                dlg.DefaultExt = ".jpg"; // Default file extension
                dlg.Filter = "jpg documents (.jpg)|*.jpg"; // Filter files by extension

                // Show save file dialog box
                Nullable<bool> result = dlg.ShowDialog();

                if (result == true)
                {
                    //Initialize RenderContext as DIB section.
                    SharpGL.RenderContextProviders.DIBSectionRenderContextProvider provider = (SharpGL.RenderContextProviders.DIBSectionRenderContextProvider)ogl.RenderContextProvider;

                    //Create drawing from GDI Bitmap.
                    Bitmap Image = Bitmap.FromHbitmap(provider.DIBSection.HBitmap);

                    //Save image.
                    Image.Save(dlg.FileName, ImageFormat.Jpeg);

                    //Display image.
                    Process.Start(dlg.FileName);
                }
            }
            //Export to PNG.
            else if (PNGExportComboBoxItem.IsSelected)
            {
                // Configure save file dialog box
                Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                dlg.FileName = "pngoutput"; // Default file name
                dlg.DefaultExt = ".png"; // Default file extension
                dlg.Filter = "png documents (.png)|*.png"; // Filter files by extension

                // Show save file dialog box
                Nullable<bool> result = dlg.ShowDialog();

                if (result == true)
                {
                    //Initialize RenderContext as DIB section.
                    SharpGL.RenderContextProviders.DIBSectionRenderContextProvider provider = (SharpGL.RenderContextProviders.DIBSectionRenderContextProvider)ogl.RenderContextProvider;

                    //Create drawing from GDI Bitmap.
                    Bitmap Image = Bitmap.FromHbitmap(provider.DIBSection.HBitmap);

                    //Save image.
                    Image.Save(dlg.FileName, ImageFormat.Png);

                    //Display image.
                    Process.Start(dlg.FileName);
                }
            }
            //Export to TIFF.
            else if (TIFFExportComboBoxItem.IsSelected)
            {
                // Configure save file dialog box
                Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                dlg.FileName = "tiffoutput"; // Default file name
                dlg.DefaultExt = ".tiff"; // Default file extension
                dlg.Filter = "tiff documents (.tiff)|*.tiff"; // Filter files by extension

                // Show save file dialog box
                Nullable<bool> result = dlg.ShowDialog();

                if (result == true)
                {
                    //Initialize RenderContext as DIB section.
                    SharpGL.RenderContextProviders.DIBSectionRenderContextProvider provider = (SharpGL.RenderContextProviders.DIBSectionRenderContextProvider)ogl.RenderContextProvider;

                    //Create drawing from GDI Bitmap.
                    Bitmap Image = Bitmap.FromHbitmap(provider.DIBSection.HBitmap);

                    //Save image.
                    Image.Save(dlg.FileName, ImageFormat.Tiff);

                    //Display image.
                    Process.Start(dlg.FileName);
                }
            }
            else this.DialogResult = false;
        }
    }
}
