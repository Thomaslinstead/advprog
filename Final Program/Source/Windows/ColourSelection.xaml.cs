﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SharpGLWPFApplication1.Windows
{
    /// <summary>
    /// Interaction logic for ColourSelection.xaml
    /// </summary>
    public partial class ColourSelection : Window
    {
        //Constructor.
        public ColourSelection()
        {
            InitializeComponent();
        }

        //Cancel button logic.
        void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            // Dialog box canceled
            this.DialogResult = false;
        }

        //OK button logic.
        void okButton_Click(object sender, RoutedEventArgs e)
        {
            // Don't accept the dialog box if there is invalid data
            //if (!IsValid(this)) return;
            try
            {
                if (int.Parse(RTextBox.Text) < 0 || int.Parse(RTextBox.Text) > 255) return;
                if (int.Parse(GTextBox.Text) < 0 || int.Parse(GTextBox.Text) > 255) return;
                if (int.Parse(BTextBox.Text) < 0 || int.Parse(BTextBox.Text) > 255) return;
                if (int.Parse(ATextBox.Text) < 0 || int.Parse(ATextBox.Text) > 255) return;
            } catch(Exception){
                return;
            }

            // Dialog box accepted
            this.DialogResult = true;
        }

        // TextChangedEventHandler delegate method. 
        private void textChangedEventHandler(object sender, TextChangedEventArgs args)
        {
            try
            {
                //Set new colour.
                ColourBox.Fill = new SolidColorBrush(Color.FromRgb(byte.Parse(RTextBox.Text), byte.Parse(GTextBox.Text), byte.Parse(BTextBox.Text)));
            }
            catch (Exception)
            {
            }
        } 
    }
}
