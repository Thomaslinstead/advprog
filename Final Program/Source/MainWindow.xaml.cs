﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel; 
using System.Windows;
using Microsoft.Win32;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SharpGL.SceneGraph;
using SharpGL;
using System.IO;
using SharpGL.SceneGraph.Collections;
using System.Drawing;
using System.Drawing.Imaging;
using System.Diagnostics;

namespace SharpGLWPFApplication1
{
    /// <summary>
    /// Interaction logic for GUI Elements and MainWindow.xaml. Handles OpenGL methods.
    /// </summary>

    public partial class MainWindow : Window
    {
        //Mouse controls
        private float mouseX;
        private float mouseY;
        private bool leftMouseDown = false;
        private bool drawStarted = false;
        private float drawStartX;
        private float drawStartY;

        //Enum types
        private enum selected { Line, Box, Circle, Curve, Text, Image, DimensionLine, Chair_side, Chair_top, Door_side, Door_top, Window_side, Window_top, InteriorWall, ExteriorWall, Stairs_side, Stairs_top, Select };
        selected currentSelection = selected.Select;
        private enum linetype { Solid, Dotted, Dashed };
        linetype currentLineType = linetype.Solid;
        private enum mode {Draw, Select, Resize, Move, Pan, Delete };
        mode currentMode = mode.Select;

        //Shape data
        private String currentText;
        private String imagePath;
        private Colour currentColour;

        //Storage list
        private ShapeList list = new ShapeList();
        
        //Singletons
        private Grid grid;
        private OpenGL gl;
        private SaveWork save;
        private LoadWork load;
        private Select sel;

        //Type identifiers
        private int doortype = 0;
        private int windowtype = 0;
        private int chairtype = 0;
        private int stairtype = 0;
        private int offsettype = 0;
        private int doubleClick = 0;
        private float offsetAmount = (5*Grid.mm);

        //MainWindow constructor.
        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            Title = "2D CAD Architecture";
            History.Instance.createList(list);
        }

        /// <summary>
        /// Handles the OpenGLDraw event of the openGLControl1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="args">The <see cref="SharpGL.SceneGraph.OpenGLEventArgs"/> instance containing the event data.</param>
        private void openGLControl_OpenGLDraw(object sender, OpenGLEventArgs args)
        {
            OpenGL gl = openGLControl.OpenGL;

            //  Clear the color and depth buffer.
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);

            //Draw grid and scale features
            gl.LoadIdentity();
            grid.drawPage();
            grid.drawGrid((int)Width, (int)Height);
            grid.drawScale((int)Width, (int)Height);
            grid.drawMousePosition(((mouseX - grid.panX) * (grid.zoom)), (mouseY - grid.panY) * (grid.zoom)); 

            //Translate and scale following drawing elements for zoom and pan.
            gl.Translate(grid.zoom * (grid.panX * -1), grid.zoom*(grid.panY * -1), 0);
            gl.Scale(grid.zoom, grid.zoom, grid.zoom);
           
            //Draw all stored shapes
            list.drawShapes();

            //Redefine the current cursor
            setCursor();


            if (currentMode == mode.Draw) //Draw Mode
            {
                
                // Process Line Type
                ILine lineStyle;
                switch (currentLineType)
                {
                    case linetype.Solid:
                        lineStyle = new SolidLine();
                        break;
                    case linetype.Dotted:
                        lineStyle = new DottedLine();
                        break;
                    case linetype.Dashed:
                        lineStyle = new DashedLine();
                        break;
                    default:
                        lineStyle = new SolidLine();
                        break;
                }

                // Process Shape
                Shape s;
                switch (currentSelection)
                {
                    case selected.Line:
                        s = new Line(drawStartX, drawStartY, mouseX, mouseY);
                        break;
                    case selected.Box:
                        s = new Square(drawStartX, drawStartY, mouseX, mouseY);
                        break;
                    case selected.Circle:
                        s = new Circle(drawStartX, drawStartY, mouseX, mouseY);
                        break;
                    case selected.Curve:
                        s = new Curve(drawStartX, drawStartY, mouseX, mouseY);
                        break;
                    case selected.Text:
                        s = new Text(drawStartX, drawStartY, mouseX, mouseY, currentText);
                        break;
                    case selected.Image:
                        s = new Image(drawStartX, drawStartY, mouseX, mouseY, Textures.Instance.textureCount()-1, imagePath);
                        break;
                    case selected.DimensionLine:
                        s = new DimensionLine(drawStartX, drawStartY, mouseX, mouseY);
                        break;
                    case selected.Chair_top:
                        s = new Chair_top(drawStartX, drawStartY, mouseX, mouseY);
                        break;
                    case selected.Chair_side:
                        s = new Chair_side(drawStartX, drawStartY, mouseX, mouseY);
                        break;
                    case selected.Door_side:
                        s = new Door_side(drawStartX, drawStartY, mouseX, mouseY);
                        break;
                    case selected.Door_top:
                        s = new Door_top(drawStartX, drawStartY, mouseX, mouseY);
                        break;
                    case selected.Window_side:
                        s = new Window_side(drawStartX, drawStartY, mouseX, mouseY);
                        break;
                    case selected.Window_top:
                        s = new Window_top(drawStartX, drawStartY, mouseX, mouseY);
                        break;
                    case selected.InteriorWall:
                        s = new InteriorWall(drawStartX, drawStartY, mouseX, mouseY);
                        break;
                    case selected.ExteriorWall:
                        s = new ExteriorWall(drawStartX, drawStartY, mouseX, mouseY);
                        break;
                    case selected.Stairs_side:
                        s = new Stairs_side(drawStartX, drawStartY, mouseX, mouseY);
                        break;
                    case selected.Stairs_top:
                        s = new Stairs_top(drawStartX, drawStartY, mouseX, mouseY);
                        break;
                    case selected.Select:
                        s = new Line(drawStartX, drawStartY, mouseX, mouseY);
                        break;
                    default:
                        s = new Line(drawStartX, drawStartY, mouseX, mouseY);
                        break;
                }

                //Set Shape Line Style
                s.colour = new Colour(currentColour.R, currentColour.G, currentColour.B);
                s.LineType = lineStyle;
                s.DrawMode = new OpenGLDraw(); //Draw using OpenGL
                
                //Draw while mouse is down and save once released
                if (leftMouseDown == true)
                {
                    if (drawStarted == false)
                    {
                        Point start = grid.gridPoint(mouseX, mouseY);
                        drawStartX = (float)start.X;
                        drawStartY = (float)start.Y;
                        s.BBoxMin = start;
                        drawStarted = true;
                    }
                    s.draw(); //Draw 
                }
                else
                {
                    if (drawStarted == true)
                    {
                        Point end = grid.gridPoint(mouseX, mouseY);
                        s.BBoxMax = end;
                        History.Instance.history.action(new AddShapeMemento()); //Change state momento
                        list.addShape((Shape)s); //Add to shape list
                        drawStarted = false;
                    }
                }

            }
            else if (currentMode == mode.Pan)  //Pan instead of draw
            {
                if (leftMouseDown == true)  //Get Pan amount when released
                {
                    if (drawStarted == false)
                    {
                        Point start = grid.gridPoint(mouseX, mouseY);
                        drawStartX = mouseX;
                        drawStartY = mouseY;
                        drawStarted = true;
                    }
                }
                else
                {
                    if (drawStarted == true)
                    {
                        grid.panX += ((10 * Grid.mm) * (int)((drawStartX - mouseX) / (10 * Grid.mm)));
                        grid.panY += ((10 * Grid.mm) * (int)((drawStartY - mouseY) / (10 * Grid.mm)));
                        drawStarted = false;
                    }
                }
            }
            else if (currentMode == mode.Select) //Select instead of draw
            {
                if (leftMouseDown == true)
                {
                    if (drawStarted == false)
                    {
                        Point start = grid.gridPoint(mouseX, mouseY);
                        drawStartX = mouseX;
                        drawStartY = mouseY;
                        drawStarted = true;
                    }

                    //Resize or move
                    if(Select.Instance.resize(list, mouseX, mouseY) == false) 
                     Select.Instance.move(list, mouseX - drawStartX, mouseY - drawStartY);
                }
                else
                {
                    if (drawStarted == true)
                    {
                        if (Select.Instance.resize(list, mouseX, mouseY) == false)
                        {
                            Select.Instance.saveMovement(list, mouseX - drawStartX, mouseY - drawStartY); //Save Movement
                        }
                        else
                            Select.Instance.resizeFinished(list); //Save Resize
                        drawStarted = false;
                    }
                }
            }
        }

        /// <summary>
        /// Handles the OpenGLInitialized event of the openGLControl1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="args">The <see cref="SharpGL.SceneGraph.OpenGLEventArgs"/> instance containing the event data.</param>
        private void openGLControl_OpenGLInitialized(object sender, OpenGLEventArgs args)
        {

            //Initialise OpenGL here.
            OpenGL gl = openGLControl.OpenGL;
            OpenGLHandler glInstance = OpenGLHandler.Instance;
            glInstance.setOpenGLReference(gl);

            //Initialise Grid
            grid = Grid.Instance;

            //Set black as default colour
            currentColour = new Colour(0, 0, 0);

            //Set the clear color.
            gl.ClearColor(0.7f, 0.7f, 0.7f, 0.0f);

            //Setup Anti-aliasing
	        gl.Enable(OpenGL.GL_LINE_SMOOTH);
            gl.Enable(OpenGL.GL_BLEND);
            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_SRC_ALPHA);
            gl.Hint(OpenGL.GL_POINT_SMOOTH_HINT, OpenGL.GL_DONT_CARE);

            //Prepare to save or load
            save = SaveWork.Instance;
            load = LoadWork.Instance;

            //Prepare ability to select lines
            sel = Select.Instance;
        }

        // Handles the Resized event of the openGLControl1 control.
        private void OpenGLControl_Resized(object sender, OpenGLEventArgs args)
        {
            //  Get the OpenGL object.
            gl = openGLControl.OpenGL;

            // Reset the current viewport
            gl.Viewport(0, 0, (int)Width, (int)Height);

            //  Set the projection matrix.
            gl.MatrixMode(OpenGL.GL_PROJECTION);

            //  Load the identity.
            gl.LoadIdentity();

            //  Create a orthographic transformation.
            gl.Ortho2D(0.0, Width, Height, 0.0); // set the coordinate system for the window

            //  Set the modelview matrix.
            gl.MatrixMode(OpenGL.GL_MODELVIEW);
            gl.LoadIdentity();
        }


        //Set style of cursor
        private void setCursor()
        {
            if (mouseX+(-1*grid.panX) < 0 || mouseY+(-1*grid.panY) < 0) //On side menu use arrow
            {
                this.Cursor = Cursors.Arrow;
            }
            else
            {
                if (currentMode == mode.Select) //Selection hand
                {
                    this.Cursor = Cursors.Hand;
                }
                if (currentMode == mode.Pan) //Pan hand
                {
                    this.Cursor = Cursors.SizeAll;
                }
            }
        }

        //Run when mouse moves and updates statistics info
        protected override void OnMouseMove(MouseEventArgs e) 
        {
            mouseX = (float)((e.GetPosition(this).X - (87) + (grid.panX * grid.zoom)) / grid.zoom); //Get mouseX position after transformations
            mouseY = (float)(Height + -1 * ((float)Height - (float)e.GetPosition(this).Y + 18 - (grid.panY * grid.zoom))) / grid.zoom; //Get mouseY position after transformations
            String t = "X: " + mouseX + " | Y:" + mouseY + " | Current Selection: " + currentSelection.ToString(); //Mouse data
            t += " | Total Shapes: " + list.size(); //Add total shape data
            t += " | Zoom: x" + grid.zoom; //Add zoom data
            if (leftMouseDown == true && currentMode == mode.Draw && mouseX > 0 && mouseY > 0) //Currently drawing
            {
                double length = 0, length2 = 0;
                if (currentSelection == selected.Line) //Line length
                {
                    length = (double)(Math.Sqrt(((mouseX - drawStartX) * (mouseX - drawStartX)) + ((mouseY - drawStartY) * (mouseY - drawStartY)))) / 2.8571428571428571428571428571429;
                }
                else if(currentSelection == selected.Circle){ //Circle radius
                    length = (((Math.Abs(mouseX - drawStartX) / 2) / 2.8571428571428571428571428571429));
                } else {
                    length = Math.Abs(drawStartX - mouseX) / 2.8571428571428571428571428571429; //Width
                    length2 = Math.Abs(drawStartY - mouseY) / 2.8571428571428571428571428571429; //Height
                }

                String text = (string.Format("{0:0.00}", (length/10) * Grid.Instance.scale)) + "cm"; //Format values
                if (length >= 100 || length <= -100)
                {
                    length = length / 100;
                    text = (string.Format("{0:0.00}", length)) + "m";
                }

                if(currentSelection == selected.Line){ //Add length and angle
                    t += " | Length: " + text;
                    t += " | Angle: " + (string.Format("{0:0.00}", (double)(Math.Atan2((drawStartY - mouseY), (drawStartX - mouseX))) * (180 / Math.PI) * -1));
                }
                else if (currentSelection == selected.Circle) //Add radius
                {
                    t += " | Radius:" + text;
                }
                else
                {
                    String text2 = (string.Format("{0:0.00}", (length2 / 10) * Grid.Instance.scale)) + "cm"; //Format and add width&height.
                    if (length2 >= 100 || length2 <= -100)
                    {
                        length2 = length2 / 100;
                        text = (string.Format("{0:0.00}", length2)) + "m";
                    }
                    t += " | Width: " + text;
                    t += " | Height: " + text;
                }
            }
            textBlock1.Text = t+"   "; //Offset from edge
        }

        //Run when mouse is pressed
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {

            if ((mouseX < grid.pageLimitX && mouseY < grid.pageLimitY && mouseX > 0 && mouseY > 0) || currentMode != mode.Draw)
            {
                leftMouseDown = true;
            }
            if (currentMode == mode.Select && list.size() > 0)
            {
                if (sel.select(mouseX, mouseY, list) == false)
                {
                    if (doubleClick >= 2) //Double click for deselect
                    {
                        sel.deselectAll(list);
                        doubleClick = 0;
                    }
                    else
                    {
                        doubleClick++;
                    }
                }
            }
        }

        //Run when mouse is released
        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            if (mouseX < grid.pageLimitX && mouseY < grid.pageLimitY && mouseX > 0 && mouseY > 0 || currentMode != mode.Draw)
            {
                leftMouseDown = false;
            }
        }

        //'File' menu items.
        //'New' menu item logic.
        void NewClick(object sender, RoutedEventArgs e)
        {
            // If the document needs to be saved
            if (list.size() > 0)
            {
                // Configure the message box
                string messagetext = "This document needs to be saved. Click Yes to save and begin a new document, No to begin a new document without saving, or Cancel to resume the current document.";
                string caption = "Save Changes?";
                MessageBoxButton button = MessageBoxButton.YesNoCancel;
                MessageBoxImage icon = MessageBoxImage.Warning;

                // Display message box
                MessageBoxResult messageBoxResult = MessageBox.Show(messagetext, caption, button, icon);

                // Process message box results
                switch (messageBoxResult)
                {
                    case MessageBoxResult.Yes: // Save document and start new.
                        save.saveClick(list, load.fileName);
                        list.clearList();
                        break;
                    case MessageBoxResult.No: // Start new without saving.
                        list.clearList();
                        break;
                    case MessageBoxResult.Cancel: // Don't start new.
                        break;
                }
            }
        }

        //'Open' menu item logic.
        private void OpenClick(object sender, RoutedEventArgs e)
        {
            load.loadClick(list); //Runs load method.
            Title = "2D CAD Architecture - " + load.fileName; //Sets current title.
            setResizeMenuItemChecked(PDFExport.Instance.getPageSize());
            setReorientateMenuItemChecked(PDFExport.Instance.getPageOrientation());

        }

        //'Save' menu item logic.
        private void SaveClick(object sender, EventArgs e)
        {
            //If file does not exist.
            if (load.fileName != null)
            {
                save.save(load.filePath, list); //Run save method. Uses filepath to save to same location.

                // Configure the message box
                string messagetext = load.fileName + " Saved.";
                string caption = "Save Complete";
                MessageBoxButton button = MessageBoxButton.OK;
                MessageBoxImage icon = MessageBoxImage.Information;

                // Display message box
                MessageBoxResult messageBoxResult = MessageBox.Show(messagetext, caption, button, icon);
            }
            else
            {
                //Do normal save if file does not already exist.
                load.fileName = save.saveClick(list, load.fileName);
                Title = "2D CAD Architecture - " + load.fileName;
            }
        }

        //'Save As' menu item logic.
        private void SaveAsClick(object sender, RoutedEventArgs e)
        {
            load.fileName = save.saveClick(list, load.fileName); //Run save method.
            Title = "2D CAD Architecture - " + load.fileName; //Set title of file.

        }

        //Increment Zoom 
        private void IncreaseZoom(object sender, RoutedEventArgs e)
        {
            if (grid.zoom < 1) //Below 1 add half each time
                if (grid.zoom + (grid.zoom * 2) < 1)
                    grid.zoom = (grid.zoom * 2);
                else
                    grid.zoom = 1;
            else
                grid.zoom = grid.zoom + 1.0f; //Over 1 add 1 each time
        }

        //Decrement Zoom 
        private void DecreaseZoom(object sender, RoutedEventArgs e)
        {
            if (grid.zoom - 0.5 <= 0.5) //Below 1 half each time
            {
                if (grid.zoom > (float)0.05F) //Stop going too small
                {
                    grid.zoom = grid.zoom - (grid.zoom / 2);
                }
            }
            else
            {
                grid.zoom = grid.zoom - 1.0f; //Over 1 take 1 each time
            }
        }

        //'ExportTo' menu item logic.
        private void exportToClick(object sender, RoutedEventArgs e)

        {
            //Instatitate new export window.
            Windows.ExportWindow e1 = new Windows.ExportWindow(list, gl);

            e1.Owner = this;

            //Show export window.
            e1.ShowDialog();
        }

        //'Exit' menu item logic.
        private void Exit_Click(object sender, EventArgs e)
        {
            this.Close(); //Close window (checks for save state).
        }


        //'Edit' menu items.
        //'Undo' menu item logic.
        private void UndoClick(object sender, EventArgs e)
        {
            if (History.Instance.history.CanUndo)
            {
                Select.Instance.deselectAll(list); //Stop selecting item to allow them to be changed. (else system will crash)
                History.Instance.history.undo();
            }
        }

        //'Redo' menu item logic.
        private void RedoClick(object sender, EventArgs e)
        {
            if (History.Instance.history.CanRedo)
            {
                Select.Instance.deselectAll(list); //Stop selecting item to allow them to be changed. (else system will crash)
                History.Instance.history.redo();
            }
        }

        //'Copy' menu item logic.
        private void CopyClick(object sender, EventArgs e)
        {
            Select.Instance.copy(mouseX, mouseY, list);
        }

        //'Cut' menu item logic.
        private void CutClick(object sender, EventArgs e)
        {
            Select.Instance.cut(mouseX, mouseY, list);
        }

        //'Paste menu item logic.
        private void PasteClick(object sender, EventArgs e)
        {
            Select.Instance.paste(mouseX, mouseY, list);
        }


        //'View' menu items.
        //'Default' menu item logic.
        private void DefaultZoom(object sender, RoutedEventArgs e)
        {
            grid.zoom = 1.0f;
        }


        //'Window' menu items.
        //'Page resize' menu and items.
        private void setResizeMenuItemChecked(int id)
        {
            //Set all checkboxes to false by default.
            A0.IsChecked = false;
            A1.IsChecked = false;
            A2.IsChecked = false;
            A3.IsChecked = false;
            A4.IsChecked = false;
            A5.IsChecked = false;

            //Set item to checked if necessary.
            switch (id)
            {
                case 0:
                    A0.IsChecked = true;
                    break;
                case 1:
                    A1.IsChecked = true;
                    break;
                case 2:
                    A2.IsChecked = true;
                    break;
                case 3:
                    A3.IsChecked = true;
                    break;
                case 4:
                    A4.IsChecked = true;
                    break;
                case 5:
                    A5.IsChecked = true;
                    break;
                default:
                    A4.IsChecked = true;
                    break;
            }
        }

        //'Orientation' menu and items.
        private void setReorientateMenuItemChecked(bool id)
        {
            //Flase by default.
            Landscape.IsChecked = false;
            Portrait.IsChecked = false;

            //Set to true if necessary.
            if (id == false)
                Landscape.IsChecked = true;
            else
                Portrait.IsChecked = true;
        }

        //'Resize' menu items logic.
        private void ResizePage(object sender, RoutedEventArgs e)
        {
            if (sender.Equals(A0)) { grid.setPageSize(1189, 841); setResizeMenuItemChecked(0); PDFExport.Instance.setPageSize(0); }
            else if (sender.Equals(A1)) { grid.setPageSize(841, 594); setResizeMenuItemChecked(1); PDFExport.Instance.setPageSize(1); }
            else if (sender.Equals(A2)) { grid.setPageSize(594, 420); setResizeMenuItemChecked(2); PDFExport.Instance.setPageSize(2); }
            else if (sender.Equals(A3)) { grid.setPageSize(420, 297); setResizeMenuItemChecked(3); PDFExport.Instance.setPageSize(3); }
            else if (sender.Equals(A4)) { grid.setPageSize(297, 210); setResizeMenuItemChecked(4); PDFExport.Instance.setPageSize(4); }
            else if (sender.Equals(A5)) { grid.setPageSize(210, 148); setResizeMenuItemChecked(5); PDFExport.Instance.setPageSize(5); }
            if (Portrait.IsChecked) grid.setPageSize(grid.pageLimitY / Grid.mm, grid.pageLimitX / Grid.mm);
        }

        //'Orientation' menu items logic.
        private void ReorientatePage(object sender, RoutedEventArgs e)
        {
            if (sender.Equals(Landscape)) { setReorientateMenuItemChecked(false); PDFExport.Instance.setPageOrientation(false); }
            else if (sender.Equals(Portrait)) { setReorientateMenuItemChecked(true); PDFExport.Instance.setPageOrientation(true); }
            if (grid.pageLimitX > grid.pageLimitY && sender.Equals(Portrait)) grid.setPageSize(grid.pageLimitY / Grid.mm, grid.pageLimitX / Grid.mm);
            if (grid.pageLimitX < grid.pageLimitY && sender.Equals(Landscape)) grid.setPageSize(grid.pageLimitY / Grid.mm, grid.pageLimitX / Grid.mm);
        }



        //'Help' menu items.
        //'Help' menu item and logic.
        private void HelpClick(object sender, EventArgs e)
        {

        }

        //'About' menu item and logic.
        private void aboutClick(object sender, RoutedEventArgs e)
        {
            //Instatiate new 'About' window
            Windows.About a1 = new Windows.About();

            a1.Owner = this;

            //Show window.
            a1.ShowDialog();
        }




        //Button logic.
        //'External Wall' button logic.
        private void extWallButton_Click(object sender, RoutedEventArgs e)
        {
            currentMode = mode.Draw; //Set current mode to Draw.
            currentSelection = selected.ExteriorWall; //Set draw selection to ExteriorWall
        }

        private void intWallButton_Click(object sender, RoutedEventArgs e)
        {
            currentMode = mode.Draw;
            currentSelection = selected.InteriorWall;
        }

        private void dimsButton_Click(object sender, RoutedEventArgs e)
        {
            currentMode = mode.Draw;
            currentSelection = selected.DimensionLine;
        }

        //Chair buttons methods.
        private void chairButton_Click(object sender, RoutedEventArgs e) //When main Chair button is clicked.
        {
            currentMode = mode.Draw; //Start draw mode.
            if (chairtype == 0) //If chair type is 0
                currentSelection = selected.Chair_top; //Draw top view version.
            else if (chairtype == 1)
                currentSelection = selected.Chair_side; //Else draw side view version.
        }
        private void chairButtonDrop_Click(object sender, RoutedEventArgs e) //Chair drop down button.
        {
            chairleftclickcontext.PlacementTarget = this; //Set context menu position.
            chairleftclickcontext.IsOpen = true; //Set context menu visibility.
        }
        private void chairButtonDrop_Click0(object sender, RoutedEventArgs e) //Chair drop down option 0.
        {
            chairtype = 0; //Set chairtype to 0.
            chairButton.Focus(); //Focus on chairbutton.
            cbdc0Menu.IsChecked = true; //Set this menu checkbox to true.
            cbdc1Menu.IsChecked = false; //Set alternate menu checkbox to false.
            cbdc0.IsChecked = true; //Set this checkbox to true.
            cbdc1.IsChecked = false; //Set alternate checkbox to false.
            currentMode = mode.Draw; //Set draw mode.
            currentSelection = selected.Chair_top; //Draw top view chair.
        }
        private void chairButtonDrop_Click1(object sender, RoutedEventArgs e) //Chair drop down option 1.
        {
            chairtype = 1; //Set chairtype to 1.
            chairButton.Focus(); //Set focus to chairbutton.
            cbdc1Menu.IsChecked = true; //Set this menu checkbox to true.
            cbdc0Menu.IsChecked = false; //Set alternate menu checkbox to false.
            cbdc1.IsChecked = true; //Set this checkbox to true.
            cbdc0.IsChecked = false; //Set alternate checkbox to false.
            currentMode = mode.Draw; //Set draw mode.
            currentSelection = selected.Chair_side; //Draw side view chair.
        }

        //Offset buttons methods.
        private void offsetButton_Click(object sender, RoutedEventArgs e)
        {
            if (offsettype == 0)
            {
                if (sel.offsetLeft(offsetAmount, list) == 0)
                {
                    // Configure the message box
                    string messagetext = "It is not possible to offset images or text.";
                    string caption = "Warning";
                    MessageBoxButton button = MessageBoxButton.OK;
                    MessageBoxImage icon = MessageBoxImage.Warning;

                    // Display message box
                    MessageBoxResult messageBoxResult = MessageBox.Show(messagetext, caption, button, icon);
                }
            }
            else if (offsettype == 1)
            {
                if (sel.offsetRight(offsetAmount, list) == 0)
                {
                    // Configure the message box
                    string messagetext = "It is not possible to offset images or text.";
                    string caption = "Warning";
                    MessageBoxButton button = MessageBoxButton.OK;
                    MessageBoxImage icon = MessageBoxImage.Warning;

                    // Display message box
                    MessageBoxResult messageBoxResult = MessageBox.Show(messagetext, caption, button, icon);
                }
            }
        }
        private void offsetButtonDrop_Click(object sender, RoutedEventArgs e)
        {
            offsetleftclickcontext.PlacementTarget = this;
            offsetleftclickcontext.IsOpen = true;
        }
        private void offsetAmount_Click(object sender, RoutedEventArgs e)
        {
            offsetleftclickcontext.PlacementTarget = this;
            offsetleftclickcontext.IsOpen = true;
            offsetAmount = (float)Convert.ToDouble(offsetText.Text) * Grid.mm; //Set offset amount.
        }
        private void offsetButtonDrop_Click0(object sender, RoutedEventArgs e)
        {
            offsettype = 0;
            if (sel.offsetLeft(offsetAmount, list) == 0)
            {
                // Configure the message box
                string messagetext = "It is not possible to offset images or text.";
                string caption = "Warning";
                MessageBoxButton button = MessageBoxButton.OK;
                MessageBoxImage icon = MessageBoxImage.Warning;

                // Display message box
                MessageBoxResult messageBoxResult = MessageBox.Show(messagetext, caption, button, icon);
            }
            obdc0Menu.IsChecked = true;
            obdc1Menu.IsChecked = false;
            odbc0.IsChecked = true;
            odbc1.IsChecked = false;
        }
        private void offsetButtonDrop_Click1(object sender, RoutedEventArgs e)
        {
            offsettype = 1;
            if (sel.offsetRight(offsetAmount, list) == 0)
            {
                // Configure the message box
                string messagetext = "It is not possible to offset images or text.";
                string caption = "Warning";
                MessageBoxButton button = MessageBoxButton.OK;
                MessageBoxImage icon = MessageBoxImage.Warning;

                // Display message box
                MessageBoxResult messageBoxResult = MessageBox.Show(messagetext, caption, button, icon);
            }
            odbc1.IsChecked = true;
            odbc0.IsChecked = false;
            obdc1Menu.IsChecked = true;
            obdc0Menu.IsChecked = false;
        }

        //Door buttons methods.
        private void doorButton_Click(object sender, RoutedEventArgs e)
        {
            currentMode = mode.Draw; //Start draw mode.
            if (doortype == 0) //If door type is 0
                currentSelection = selected.Door_top; //Draw top view version.
            else if (doortype == 1)
                currentSelection = selected.Door_side; //Else draw side view version.

        }
        private void doorButtonDrop_Click(object sender, RoutedEventArgs e)
        {
            doorleftclickcontext.PlacementTarget = this;
            doorleftclickcontext.IsOpen = true;
        }
        private void doorButtonDrop_Click0(object sender, RoutedEventArgs e)
        {
            doortype = 0;
            doorButton.Focus();
            dbdc0Menu.IsChecked = true;
            dbdc1Menu.IsChecked = false;
            dbdc0.IsChecked = true;
            dbdc1.IsChecked = false;
            currentMode = mode.Draw;
            currentSelection = selected.Door_top;
        }
        private void doorButtonDrop_Click1(object sender, RoutedEventArgs e)
        {
            doortype = 1;
            doorButton.Focus();
            dbdc1Menu.IsChecked = true;
            dbdc0Menu.IsChecked = false;
            dbdc1.IsChecked = true;
            dbdc0.IsChecked = false;
            currentMode = mode.Draw;
            currentSelection = selected.Door_side;
        }

        //Window Buttons Methods.
        private void windowButton_Click(object sender, RoutedEventArgs e)
        {
            currentMode = mode.Draw; //Start draw mode.
            if (windowtype == 0) //If window type is 0
                currentSelection = selected.Window_top; //Draw top view version.
            else if (windowtype == 1)
                currentSelection = selected.Window_side; //Else draw side view version.
        }
        private void windowButtonDrop_Click(object sender, RoutedEventArgs e)
        {
            windowleftclickcontext.PlacementTarget = this;
            windowleftclickcontext.IsOpen = true;
        }
        private void windowButtonDrop_Click0(object sender, RoutedEventArgs e)
        {
            windowtype = 0;
            windowButton.Focus();
            wbdc0Menu.IsChecked = true;
            wbdc1Menu.IsChecked = false;
            wbdc0.IsChecked = true;
            wbdc1.IsChecked = false;
            currentMode = mode.Draw;
            currentSelection = selected.Window_top;
        }
        private void windowButtonDrop_Click1(object sender, RoutedEventArgs e)
        {
            windowtype = 1;
            windowButton.Focus();
            wbdc1Menu.IsChecked = true;
            wbdc0Menu.IsChecked = false;
            wbdc1.IsChecked = true;
            wbdc0.IsChecked = false;
            currentMode = mode.Draw;
            currentSelection = selected.Window_side;
        }

        //Stair buttons methods.
        private void stairButton_Click(object sender, RoutedEventArgs e)
        {
            currentMode = mode.Draw; //Start draw mode.
            if (stairtype == 0) //If stair type is 0
                currentSelection = selected.Stairs_top; //Draw top view version.
            else if (stairtype == 1)
                currentSelection = selected.Stairs_side; //Else draw side view version.
        }
        private void stairButtonDrop_Click(object sender, RoutedEventArgs e)
        {
            stairleftclickcontext.PlacementTarget = this;
            stairleftclickcontext.IsOpen = true;
        }
        private void stairButtonDrop_Click0(object sender, RoutedEventArgs e)
        {
            stairtype = 0;
            stairButton.Focus();
            sbdc0Menu.IsChecked = true;
            sbdc1Menu.IsChecked = false;
            sbdc0.IsChecked = true;
            sbdc1.IsChecked = false;
            currentMode = mode.Draw;
            currentSelection = selected.Stairs_top;
        }
        private void stairButtonDrop_Click1(object sender, RoutedEventArgs e)
        {
            stairtype = 1;
            stairButton.Focus();
            sbdc1Menu.IsChecked = true;
            sbdc0Menu.IsChecked = false;
            sbdc1.IsChecked = true;
            sbdc0.IsChecked = false;
            currentMode = mode.Draw;
            currentSelection = selected.Stairs_side;
        }


        //'Line' button logic.
        private void lineButton_Click(object sender, RoutedEventArgs e)
        {
            currentMode = mode.Draw; //Set mode to Draw.
            currentSelection = selected.Line; //Set draw type to Line.
        }

        //'Square' button logic.
        private void squareButton_Click(object sender, RoutedEventArgs e)
        {
            currentMode = mode.Draw;
            currentSelection = selected.Box;
        }

        //'Circle' button logic.
        private void circleButton_Click(object sender, RoutedEventArgs e)
        {
            currentMode = mode.Draw;
            currentSelection = selected.Circle;
        }

        //'Arc' button logic.
        private void arcButton_Click(object sender, RoutedEventArgs e)
        {
            currentMode = mode.Draw;
            currentSelection = selected.Curve;
        }

        //'Image' button logic.
        private void imageButton_Click(object sender, RoutedEventArgs e)
        {
            currentMode = mode.Draw;
            currentSelection = selected.Image;
            imagePath = load.loadFilePath();
            if (Textures.Instance.initializeTexture(imagePath) == false)
                currentSelection = selected.Line;
        }

        //'Text' button logic.
        private void textButton_Click(object sender, RoutedEventArgs e)
        {
            currentMode = mode.Draw;
            currentSelection = selected.Text;
            currentText = Microsoft.VisualBasic.Interaction.InputBox("Please enter text in the box below:", "New Text Field", "Type Here"); //Displays a prompt input box.
        }


        //'Line Style' combo box logic.
        private void lineStyleComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lineStyleComboBox3.IsSelected) //If item 3 is selected.
            {
                currentLineType = linetype.Dashed; //Set line typle to Dashed.
            }
            else if (lineStyleComboBox2.IsSelected)
            {
                currentLineType = linetype.Dotted;
            }
            else currentLineType = linetype.Solid;
        }


        //'Line Colour' combo box logic.
        private void lineColourComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lineColourComboBoxBlack.IsSelected) //If black selected.
            {
                currentColour.setColour(0, 0, 0); //Set colour accordingly.
                lineColourComboBox.Items.Remove(lineColourComboBoxAuto); //Remove Auto option from combobox list.
            }

            else if (lineColourComboBoxRed.IsSelected)
            {
                currentColour.setColour(255, 0, 0);
                lineColourComboBox.Items.Remove(lineColourComboBoxAuto);
            }
            else if (lineColourComboBoxGreen.IsSelected)
            {
                currentColour.setColour(0, 255, 0);
                lineColourComboBox.Items.Remove(lineColourComboBoxAuto);
            }
            else if (lineColourComboBoxBlue.IsSelected)
            {
                currentColour.setColour(0, 0, 255);
                lineColourComboBox.Items.Remove(lineColourComboBoxAuto);
            }
            else if (lineColourComboBoxYellow.IsSelected)
            {
                currentColour.setColour(255, 255, 0);
                lineColourComboBox.Items.Remove(lineColourComboBoxAuto);
            }
            else if (lineColourComboBoxCyan.IsSelected)
            {
                currentColour.setColour(0, 255, 255);
                lineColourComboBox.Items.Remove(lineColourComboBoxAuto);
            }
            else if (lineColourComboBoxOther.IsSelected) //If Other(...) selected.
            {
                lineColourComboBox.Items.Remove(lineColourComboBoxAuto);

                // Instantiate the dialog box
                Windows.ColourSelection cl = new Windows.ColourSelection();

                // Configure the dialog box
                cl.Owner = this;

                // Open the dialog box modally 
                cl.ShowDialog();

                //If OK pressed.
                if (cl.DialogResult == true)
                {
                    //Set colour and rectangle fill accordingly.
                    colourRectangle.Fill = new SolidColorBrush(System.Windows.Media.Color.FromRgb(byte.Parse(cl.RTextBox.Text), byte.Parse(cl.GTextBox.Text), byte.Parse(cl.BTextBox.Text)));
                    currentColour.setColour((float.Parse(cl.RTextBox.Text)) / 255, (float.Parse(cl.GTextBox.Text)) / 255, (float.Parse(cl.BTextBox.Text)) / 255);
                }
            }
        }


        //'Tools' buttons.
        //'Select' button logic.
        private void selectButton_Click(object sender, RoutedEventArgs e)
        {
            Select.Instance.deselectAll(list);
            currentMode = mode.Select; //Set currentMode to Select.
        }

        //'Delete' button logic.
        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            currentMode = mode.Delete; //Set current mode to Delete.
            sel.delete(list);//Run deletetion method.
            currentMode = mode.Select;
            selectButton.Focus();
        }

        //'Pan' button logic.
        private void panButton_Click(object sender, RoutedEventArgs e)
        {
            currentSelection = selected.Select;
            currentMode = mode.Pan;
        }




        //'Grid' items.
        //'Snap to grid' checked logic.
        private void stg_Checked(object sender, RoutedEventArgs e)
        {
            grid.snapTogrid = true; //Set snap to grid to true.
        }
        //'Snap to grid' unchecked logic.
        private void stg_Unchecked(object sender, RoutedEventArgs e)
        {
            grid.snapTogrid = false; //Set snap to grid to false.
        }
        //'Show grid' checked logic.
        private void sg_Checked(object sender, RoutedEventArgs e)
        {
            Grid.Instance.showGrid = true;
        }
        //'Show grid' unchecked logic.
        private void sg_Unchecked(object sender, RoutedEventArgs e)
        {
            Grid.Instance.showGrid = false;
        }

        //'Scale' combo box logic.
        private void scaleComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (scaleComboBox6.IsSelected) //If selection 6.
            {
                Grid.Instance.scale = 200; //Set scale.
                gridComboBox1.Content = "40cm"; //Set grid combobox item accordingly.
                gridComboBox2.Content = "1m";
                gridComboBox3.Content = "2m";
                gridComboBox4.Content = "4m";
            }
            else if (scaleComboBox5.IsSelected)
            {
                Grid.Instance.scale = 100;
                gridComboBox1.Content = "20cm";
                gridComboBox2.Content = "50cm";
                gridComboBox3.Content = "1m";
                gridComboBox4.Content = "2m";
            }
            else if (scaleComboBox4.IsSelected)
            {
                Grid.Instance.scale = 50;
                gridComboBox1.Content = "10cm";
                gridComboBox2.Content = "25cm";
                gridComboBox3.Content = "50cm";
                gridComboBox4.Content = "1m";
            }
            else if (scaleComboBox3.IsSelected)
            {
                Grid.Instance.scale = 10;
                gridComboBox1.Content = "2cm";
                gridComboBox2.Content = "5cm";
                gridComboBox3.Content = "10cm";
                gridComboBox4.Content = "20cm";
            }
            else if (scaleComboBox2.IsSelected)
            {
                Grid.Instance.scale = 5;
                gridComboBox1.Content = "1cm";
                gridComboBox2.Content = "2.5cm";
                gridComboBox3.Content = "5cm";
                gridComboBox4.Content = "10cm";
            }
            else
            {
                Grid.Instance.scale = 1;

                gridComboBox1.Content = "2mm";
                gridComboBox2.Content = "5mm";
                gridComboBox3.Content = "10mm";
                gridComboBox4.Content = "20mm";
            }
        }

        //'Grid' combobox logic.
        private void gridComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (gridComboBox4.IsSelected) //If selection 4.
            {
                Grid.Instance.gridSize = Grid.mm * 20; //Set gridSize.
            }
            else if (gridComboBox3.IsSelected)
            {
                Grid.Instance.gridSize = Grid.mm * 10;
            }
            else if (gridComboBox2.IsSelected)
            {
                Grid.Instance.gridSize = Grid.mm * 5;
            }
            else Grid.Instance.gridSize = Grid.mm * 2;
        }

        //'Zoom' buttons.
        //'Zoom in' button logic.
        private void zoomInButton_Click(object sender, RoutedEventArgs e)
        {
            if (grid.zoom < 1)
                if (grid.zoom + (grid.zoom * 2) < 1)
                    grid.zoom = (grid.zoom * 2);
                else
                    grid.zoom = 1;
            else
                grid.zoom = grid.zoom + 1.0f;
        }

        //'Zoom out' button logic.
        private void zoomOutButton_Click(object sender, RoutedEventArgs e)
        {
            if (grid.zoom - 0.5 <= 0.5)
            {
                if (grid.zoom > (float)0.05F)
                {
                    grid.zoom = grid.zoom - (grid.zoom / 2);
                }
            }
            else
            {
                grid.zoom = grid.zoom - 1.0f;
            }
        }
   




        //Window closing logic. Applys to 'Exit' menu item and 'X' on window.
        void mainWindow_Closing(object sender, CancelEventArgs e)
        {
            // If the document needs to be saved
            if (list.size() > 0)
            {
                // Configure the message box
                string messagetext = "This document needs to be saved. Click Yes to save and exit, No to exit without saving, or Cancel to not exit.";
                string caption = "Save Changes?";
                MessageBoxButton button = MessageBoxButton.YesNoCancel;
                MessageBoxImage icon = MessageBoxImage.Warning;

                // Display message box
                MessageBoxResult messageBoxResult = MessageBox.Show(messagetext, caption, button, icon);

                // Process message box results
                switch (messageBoxResult)
                {
                    case MessageBoxResult.Yes: // Save document and exit
                        if (load.fileName != null)
                            save.save(load.filePath, list);
                        else
                            save.saveClick(list, load.fileName);
                        break;
                    case MessageBoxResult.No: // Exit without saving
                        break;
                    case MessageBoxResult.Cancel: // Don't exit
                        e.Cancel = true;
                        break;
                }
            }
        }

        //OpenGL Reize method.
        private void resizedWPF(object sender, SizeChangedEventArgs e)
        {
           Width = ActualWidth;
           Height = ActualHeight;
           openGLControl.Height = Height;
           openGLControl.Width = Width;
           OpenGLHandler glInstance = OpenGLHandler.Instance;
           glInstance.Height = Height;
           glInstance.Width = Width;

           //  Get the OpenGL object.
           gl = openGLControl.OpenGL;

           // Reset the current viewport
           gl.Viewport(0, 0, (int)Width, (int)Height);

           //  Set the projection matrix.
           gl.MatrixMode(OpenGL.GL_PROJECTION);

           //  Load the identity.
           gl.LoadIdentity();

           //  Create a perspective transformation.
           gl.Ortho2D(0.0, Width, Height, 0.0); // set the coordinate system for the window

           //  Set the modelview matrix.
           gl.MatrixMode(OpenGL.GL_MODELVIEW);
           gl.LoadIdentity();
        }


        
        //Zoom for mousewheel.
        private void Zoom_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
            { //If mouse wheel moves up. (Zoom In)
                if (grid.zoom < 1)
                {
                    if (grid.zoom + (grid.zoom * 2) < 1)
                    {
                        grid.zoom = (grid.zoom * 2);
                    }
                    else
                    {
                        grid.zoom = 1;
                    }
                }
                else
                {
                    grid.zoom = grid.zoom + 1.0f;
                }
            }  //If mouse wheel moves down. (Zoom out)
            else
            {
                if (grid.zoom - 0.5 <= 0.5)
                {
                    if (grid.zoom > (float)0.05F)
                    {
                        grid.zoom = grid.zoom - (grid.zoom / 2);
                    }
                }
                else
                {
                    grid.zoom = grid.zoom - 1.0f;
                }
            }
        }
    }
}
