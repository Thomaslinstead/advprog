﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using PdfSharp.Pdf.Advanced;

namespace SharpGLWPFApplication1
{
    /* Class for handling export to PDF Format */
    class PDFExport
    {
        private static PDFExport instance; //Singleton instance variable

        internal static PdfDocument s_document; //PDF Document Object
        public XGraphics gfx; //Graphics area of document Object
        public static PdfPage page; //Specify a PDF page

        //PDF Information
        private int pageSize = 4; 
        private bool pageOrientation = false;

        /* Default Constructor */
        private PDFExport()
        {
            // Construct the default document
            s_document = new PdfDocument();
            page = new PdfPage();
            s_document.AddPage(page);
            gfx = XGraphics.FromPdfPage(page);
            setPageSize(pageSize);
            setPageOrientation(pageOrientation);
        }

        /* Singleton Instance */
        public static PDFExport Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PDFExport();
                    page.Size = PdfSharp.PageSize.A4;
                    page.Orientation = PdfSharp.PageOrientation.Landscape;
                }
                return instance;
            }
        }

        /* Export shape data to PDF format        *
         * list - List of shapes to export        */
        public void Export(ShapeList list)
        {
            //PDF Metadata
            string filename = String.Format("{0}_tempfile.pdf", Guid.NewGuid().ToString("D").ToUpper());
            s_document.Info.Title = "2D Architectural Drawing";
            s_document.Info.Author = "John Gilbey, Thomas Linstead, Sam Westlake";
            s_document.Info.Subject = "Here is an exported PDF document from 2D Architectural Drawing";
            s_document.Info.Keywords = "Architecture, PDFExport";

            //Draw all the shapes to the PDF Document
            list.drawPDFShapes();

            // Configure save file dialog box
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "pdfoutput"; // Default file name
            dlg.DefaultExt = ".pdf"; // Default file extension
            dlg.Filter = "pdf documents (.pdf)|*.pdf"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results 
            if (result == true)
            {
                filename = dlg.FileName; //Save File path
                try
                {
                    s_document.Save(filename); // Save document 
                }
                catch (Exception E)
                {
                    //Display Error
                    Microsoft.VisualBasic.Interaction.MsgBox("Error: "+E.ToString());
                }
            }                        
            Process.Start(filename); //Show final PDF in default viewer

            s_document.Dispose();
            instance = new PDFExport();
        }

        /* Getter Method for current Page Size */
        public int getPageSize()
        {
            return pageSize;
        }

        /* Getter Method for current Page Orientation */
        public bool getPageOrientation()
        {
            return pageOrientation;
        }

        /* Setter Method for current Page Size */
        public void setPageSize(int A)
        {
            pageSize = A;
            switch (A)
            {
                case 0:
                    page.Size = PdfSharp.PageSize.A0;
                    break;
                case 1:
                    page.Size = PdfSharp.PageSize.A1;
                    break;
                case 2:
                    page.Size = PdfSharp.PageSize.A2;
                    break;
                case 3:
                    page.Size = PdfSharp.PageSize.A3;
                    break;
                case 4:
                    page.Size = PdfSharp.PageSize.A4;
                    break;
                case 5:
                    page.Size = PdfSharp.PageSize.A5;
                    break;
                default:
                    page.Size = PdfSharp.PageSize.A4;
                    break;
            }

        }

        /* Setter Method for current Page Orientation */
        public void setPageOrientation(bool p)
        {
            pageOrientation = p;
            if(p){
                page.Orientation = PdfSharp.PageOrientation.Portrait;
            } else {
                page.Orientation = PdfSharp.PageOrientation.Landscape;
            }
        }

    }
}
