﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpGL;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows;
using Microsoft.Win32;

namespace SharpGLWPFApplication1
{
    /* Singleton class for loading and storing textures used within the application - Thomas Linstead */
    public sealed class Textures
    {
        //Singleton Instance
        private static Textures instance;

        //The texture identifier. Max 100 Loaded Textures!
        public uint[][] textures = new uint[100][];

        //Current Number of loaded textures
        public static int numberOfTextures;

        //Storage the current texture.
        private Bitmap textureImage;

        /*Private Constructor*/
        private Textures() { 
            
        }

        /*Singleton Instance*/
        public static Textures Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Textures();
                    numberOfTextures = 0;
                }
                return instance;
            }
        }

        /*  Load a texture into the program  *
         *  Path - File Path to Texture      */
        public bool initializeTexture(String path)
        {
            //Get the OpenGL handler
            OpenGLHandler glInstance = OpenGLHandler.Instance;
            OpenGL gl = glInstance.getOpenGLReference();

            try
            {
                if (path != null)
                {
                    //Load the bitmap texture from file.
                    textureImage = new Bitmap(path);
                    //textureImage.

                    //Initialisation here, to enable textures and reset colours.
                    gl.Enable(OpenGL.GL_TEXTURE_2D);
                    gl.Color(0.0f, 0.0f, 0.0f);

                    //Get one texture id, and stick it into the textures array.
                    uint[] tex = new uint[1];
                    gl.GenTextures(1, tex);
                    textures[numberOfTextures] = tex;

                    //Bind the texture.
                    gl.BindTexture(OpenGL.GL_TEXTURE_2D, textures[numberOfTextures][0]);

                    //Tell OpenGL where the about the texture data.
                    gl.TexImage2D(OpenGL.GL_TEXTURE_2D, 0, 4, textureImage.Width, textureImage.Height, 0, OpenGL.GL_BGRA, OpenGL.GL_UNSIGNED_BYTE,
                        textureImage.LockBits(new Rectangle(0, 0, textureImage.Width, textureImage.Height),
                        ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb).Scan0);

                    //Specify linear filtering.
                    gl.TexParameter(OpenGL.GL_TEXTURE_2D, OpenGL.GL_TEXTURE_MIN_FILTER, OpenGL.GL_LINEAR);
                    gl.TexParameter(OpenGL.GL_TEXTURE_2D, OpenGL.GL_TEXTURE_MAG_FILTER, OpenGL.GL_LINEAR);

                    //Increment the number of textures loaded
                    numberOfTextures++;
                    gl.Disable(OpenGL.GL_TEXTURE_2D);
                    return true; //Success
                }
                else
                {
                    return false; //No path
                }
            }
            catch (Exception E) //Error Occured
            {
                String error = "Invalid image file or file not found at path:\n " + path + "\n (File format: .JPG, .PNG, .GIF, .BMP etc)  \n\nDebug Message: \n" + E.ToString(); //Get the error

                // Configure error message box
                string caption = "Error while loading a texture:";
                MessageBoxButton button = MessageBoxButton.OK;
                MessageBoxImage icon = MessageBoxImage.Error;

                // Display message box
                MessageBoxResult messageBoxResult = MessageBox.Show(error, caption, button, icon);

                return false; //Failure
            }
        }

        /* Get the current number of textures loaded */
        public int textureCount()
        {
            return numberOfTextures;
        }

    }
}
