﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using SharpGL;

namespace SharpGLWPFApplication1
{
    public sealed class Grid
    {
        //Class variables
        private static Grid instance;
        public const float mm = 2.8571428571428571428571428571429F;
        public const float scaleSpacing = 2.8571428571428571428571428571429F*5;
        public double gridSize { get; set; }
        public bool showGrid { get; set; }
        public bool snapTogrid { get; set; }
        public int scale { get; set; }
        public float zoom { get; set; }
        public int pageLimitX { get; set; }
        public int pageLimitY { get; set; }
        public float panX { get; set; }
        public float panY { get; set; }

        //Constructor
        private Grid() {
            gridSize = mm*5; //0.5CM Apart
            showGrid = true;
            snapTogrid = false;
            scale = 1;
            zoom = 1;
            pageLimitX = (int)(297*mm);
            pageLimitY = (int)(210*mm);
            panX = 0;
            panY = 0;
        }

        //Get instance of class
        public static Grid Instance
            {
                get
                {
                    if (instance == null)
                    {
                        instance = new Grid();
                    }
                    return instance;
                }
            }

        //Setup initial page
        public void drawPage()
        {
            //Get OpenGL functionality
            OpenGLHandler glInstance = OpenGLHandler.Instance;
            OpenGL gl = glInstance.getOpenGLReference();
            gl.PushMatrix();
            //Set scale
            gl.Scale(zoom, zoom, zoom);
            //Set colour
            gl.Color(255.0, 255.0, 255.0);
            //Set position
            gl.Begin(OpenGL.GL_POLYGON);
            int px = (int)(panX / (10 * mm));
            int py = (int)(panY / (10 * mm));
            gl.Vertex(0 - px * (10 * mm), 0 - py * (10 * mm));
            gl.Vertex(pageLimitX - px*(10 * mm), 0 - py * (10 * mm));
            gl.Vertex(pageLimitX - px * (10 * mm), pageLimitY - py * (10 * mm));
            gl.Vertex(0 - px*(10 * mm), pageLimitY - py * (10 * mm));
            gl.End();
            gl.PopMatrix();

        }

        //Draw mouse pointer
        public void drawMousePosition(float mouseX, float mouseY)
        {
            //Get OpenGL functionality
            OpenGLHandler glInstance = OpenGLHandler.Instance;
            OpenGL gl = glInstance.getOpenGLReference();
            //Draw pointer
            gl.PushMatrix();
            gl.Begin(OpenGL.GL_LINES);
            gl.Vertex(mouseX, 0);
            gl.Vertex(mouseX, 0 + scaleSpacing);
            gl.End();
            gl.Begin(OpenGL.GL_LINES);
            gl.Vertex(0, mouseY);
            gl.Vertex(scaleSpacing, mouseY);
            gl.End();
            gl.PopMatrix();

        }

        //Draw grid on page if show grid is requested
        public void drawGrid(int Width, int Height)
        {
            if (zoom > 0.25)
            {
                //Get OpenGL functionality
                OpenGLHandler glInstance = OpenGLHandler.Instance;
                OpenGL gl = glInstance.getOpenGLReference();
                if (showGrid == true)
                {
                    //Draw grid points
                    gl.PushMatrix();
                    gl.PointSize(1);
                    gl.Begin(OpenGL.GL_POINTS);
                    gl.Color(0.7, 0.7, 0.7);
                    for (double i = 0; i + gridSize * zoom <= Width; i += gridSize * zoom)
                    {
                        for (double j = 0; j + gridSize * zoom <= Height + 2; j += gridSize * zoom)
                        {
                            gl.Vertex(i, j);
                        }
                    }
                    gl.End();
                    gl.PopMatrix();
                }
            }
        }

        //Draw scale on page
        public void drawScale(int Width, int Height)
        {
            //Get OpenGL functionality
            OpenGLHandler glInstance = OpenGLHandler.Instance;
            OpenGL gl = glInstance.getOpenGLReference();

            //Count to increase the step by step value
            int count = 0;
            gl.Color(0.2, 0.2, 0.2); //Colour slightly gray

            //Draw the X Axis Scale
            for (double i = scaleSpacing; i + scaleSpacing <= Width; i += scaleSpacing)
                {
                    gl.PushAttrib(OpenGL.GL_ENABLE_BIT);
                    if (count % 2 == 0) //Dashed dividing scale lines
                    {
                        gl.LineStipple(1, 0xBBBB);  //Dashed Line
                        gl.Enable(OpenGL.GL_LINE_STIPPLE);
                        gl.Begin(OpenGL.GL_LINES); //Draw the Line
                        gl.Vertex(i, 0);
                        gl.Vertex(i, 0 + scaleSpacing / 2);
                        gl.End();
                    }
                    else
                    {  //Longer full lines with labels
                        String faceName = "Arial";

                        int value = (count + 1) / 2 + (int)(panX / (10 * mm)); //Calculate the scale value
                        float scaleValue = value * scale;

                        if (zoom < 1) //Deal with negative zoom factors
                        {
                            value = (int)(((count + 1) / 2 + ((int)(panX / (10 * mm))*zoom))/ zoom);
                            scaleValue = value * scale;
                        }

                        //Convert to text form
                        String text = scaleValue + "cm";
                        if (scaleValue >= 100 || scaleValue <= -100)
                        {
                            scaleValue = scaleValue/100; //Meters
                            text = scaleValue + "m";
                        }

                        //Draw the text and lines
                        gl.PushMatrix();
                        if (zoom >= 1) 
                            gl.DrawText((int)(i * Grid.Instance.zoom - 3), Height - 24, 0.0f, 0.0f, 0.0f, faceName, 10.0f, text);
                        else
                            gl.DrawText((int)(i-3), Height - 24, 0.0f, 0.0f, 0.0f, faceName, 10.0f, text);
                        gl.PopMatrix();

                        gl.Begin(OpenGL.GL_LINES);
                        gl.Vertex(i, 0);
                        gl.Vertex(i, 0 + scaleSpacing);
                        gl.End();
                    }
                    gl.PopAttrib();
                    count++; //Increment the value count 
                }

            count = 0; //Reuse count for Y Axis

            //Draw the Y Axis Scale
            for (double j = 0; j + scaleSpacing <= Height + 2; j += scaleSpacing)
            {
                gl.PushAttrib(OpenGL.GL_ENABLE_BIT);
                if (count % 2 == 0 && count != 0) //Draw solid dividing lines
                {
                    gl.Begin(OpenGL.GL_LINES);
                    gl.Vertex(0, j);
                    gl.Vertex(scaleSpacing, j);
                    gl.End();
                }
                else
                {
                    gl.LineStipple(1, 0xBBBB);  //Dashed dividing scale lines
                    gl.Enable(OpenGL.GL_LINE_STIPPLE);
                    gl.Begin(OpenGL.GL_LINES);
                    gl.Vertex(0, j);
                    gl.Vertex(scaleSpacing / 2, j);
                    gl.End();
                }
                gl.PopAttrib();
                count++;
            }

            //Add the Y Axis Values (Inverse Ordered)
            count = 0;
            for (double i = Height / Grid.Instance.zoom; i + scaleSpacing > 0; i -= scaleSpacing)
            {
                if (count % 2 == 0 && count != 0) //Only add every even amount
                {
                    String faceName = "Arial";
                    int value = (count + 1) / 2 + (int)(panY/(10*mm)); //Calculate values
                    float scaleValue = value * scale;
                    String text = scaleValue + "cm";  //Setup text format
                    if (scaleValue >= 100 || scaleValue <= -100)
                    {
                        scaleValue = scaleValue / 100; //Meters
                        text = scaleValue + "m";
                    }
                    gl.PushMatrix();
                    if (zoom > 0.5) //Draw Text
                        gl.DrawText((int)18, (int)(i * Grid.Instance.zoom) - 3, 0.0f, 0.0f, 0.0f, faceName, 10.0f, text);
                    else
                        if (count % (int)(System.Math.Pow(Grid.Instance.zoom,-1.0)*2) == 0)
                        gl.DrawText((int)18, (int)(i * Grid.Instance.zoom) - 3, 0.0f, 0.0f, 0.0f, faceName, 10.0f, text);
                    gl.PopMatrix();
                }
                count++;
            }
        }

        //Set page size
        public void setPageSize(float x, float y)
        {
            pageLimitX = (int)(mm * x);
            pageLimitY = (int)(mm * y);
        }

        //Return grid point closest to co-ordinate passed
        public Point gridPoint(float x, float y)
        {
            if (snapTogrid == true)
            {
                double tempX = (int)x;
                double tempY = (int)y;
                tempX = tempX % gridSize;
                tempY = tempY % gridSize;
                if (tempX < gridSize / 2)
                    tempX = (double)x - tempX;
                else
                    tempX = (int)x + (gridSize - tempX);
                if (tempY < gridSize / 2)
                    tempY = (int)y - tempY;
                else
                    tempY = (int)y + (gridSize - tempY);
                Point point = new Point((float)tempX, (float)tempY);
                return point;
            }
            else
            {
                Point point = new Point(x, y);
                return point;
            }
        }
    }
}
