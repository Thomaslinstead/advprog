﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpGL;

namespace SharpGLWPFApplication1
{
    /* OpenGL Singleton Class */
    public sealed class OpenGLHandler
    {

        private static OpenGLHandler instance; //Singleton instance object

        OpenGL gl;                         //OpenGl Object
        public double Height { get; set; } //OpenGL window information
        public double Width { get; set; }


        //Default private constructor
        private OpenGLHandler() { 
            
        }

        /* Singleton Instance */
        public static OpenGLHandler Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new OpenGLHandler();
                }
                return instance;
            }
        }

        /* set the OpenGL object */
        public void setOpenGLReference(OpenGL glRef)
        {
            gl = glRef;
        }

        /*get the OpenGL object */
        public OpenGL getOpenGLReference(){
            return gl;
        }

    }
}
