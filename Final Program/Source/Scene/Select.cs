﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace SharpGLWPFApplication1
{
    public sealed class Select
    {
        //Class variables
        private static Select instance;
        private static float mouseX;
        private static float mouseY;
        private static Point cPosition;
        private bool min = false;
        private int index = 0;
        private struct selected
        {
            public int index;
            public float actualR;
            public float actualG;
            public float actualB;
        };
        private List<selected> selectedList;
        private ShapeList clipboardList;
        private ShapeList MoveList;

        //Constructor
        private Select()
        {
            selectedList = new List<selected>();
            MoveList = new ShapeList();
        }

        //Get instance of class
        public static Select Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Select();
                }
                return instance;
            }
        }

        //Records shapes near mouse position and returns a boolean indicating if one or shapes have been selected
        public bool select(float mouseXPos, float mouseYPos, ShapeList list)
        {
            Shape temp;
            mouseX = mouseXPos; 
            mouseY = mouseYPos;
            int size = list.size();
            bool selected = false;

            //For each shape in list, check if close enough to mouse location
            for (int i = 0; i < size; i++)
            {
                temp = list.getShape(i);
                temp.DrawMode = new SelectionDraw();
                if (temp.draw() > 0)
                {
                    //If shape is not already selected, add it to selected shapes
                    if (!alreadySelected(i))
                    {
                        selected newSelection = new selected();
                        newSelection.index = i;
                        newSelection.actualR = temp.colour.R;
                        newSelection.actualG = temp.colour.G;
                        newSelection.actualB = temp.colour.B;
                        selectedList.Add(newSelection);
                        temp.colour.setColour(256, 0, 0);
                        selected = true;
                    }
                }
            }
            return selected;
        }

        //Test if shape is already selected
        public bool alreadySelected(int index)
        {
            int shapesInList = selectedList.Count();
            for (int i = 0; i < shapesInList; i++)
            {
                if (selectedList.ElementAt(i).index == index)
                    return true;
            }
            return false;
        }

        //Deselect a single shape
        public void deselectOne(ShapeList list, int ind)
        {
            selected temp = new selected();
            temp = findInSelectedList(ind);
            list.getShape(temp.index).colour.setColour(temp.actualR, temp.actualG, temp.actualB);
            selectedList.Remove(temp);
        }

        //Deselect all shapes currently selected
        public void deselectAll(ShapeList list)
        {
            int shapesInList = selectedList.Count();
            for (int i = 0; i < shapesInList; i++)
            {
                list.getShape(selectedList.ElementAt(i).index).colour.setColour(selectedList.ElementAt(i).actualR, selectedList.ElementAt(i).actualG, selectedList.ElementAt(i).actualB);
            }
            selectedList.Clear();
        }

        //Delete any shapes that are selected from the list of shapes passed
        public void delete(ShapeList list)
        {
            //If there are selected shapes
            if (selectedList.Count > 0)
            {
                //Order shapes by their index number in main shape list
                selectedList.Sort((ref1, ref2) => ref1.index.CompareTo(ref2.index));
                //For each selected shape, remove them from main shape list
                for (int i = 0; i < selectedList.Count(); i++)
                {
                    Shape t = list.getShape(selectedList.ElementAt(i).index);
                    t.colour.setColour(selectedList.ElementAt(i).actualR, selectedList.ElementAt(i).actualG, selectedList.ElementAt(i).actualB);
                    list.deleteShape(t);
                    History.Instance.history.action(new RemoveShapeMemento(t));
                    for (int j = 0; j < selectedList.Count(); j++)
                    {
                        selected temp = new selected();
                        temp.index = selectedList.ElementAt(j).index - 1;
                        temp.actualB = selectedList.ElementAt(j).actualB;
                        temp.actualG = selectedList.ElementAt(j).actualG;
                        temp.actualR = selectedList.ElementAt(j).actualR;
                        selectedList[j] = temp;
                    }
                }
                //Clear list of selected shapes
                selectedList.Clear();
            }
        }

        //Move selected shapes
        public void move(ShapeList list, float changeInX, float changeInY)
        {
            //If there are selected shapes
            if (selectedList.Count > 0)
            {
                //Order shapes by their index number in main list
                selectedList.Sort((ref1, ref2) => ref1.index.CompareTo(ref2.index));
                //For each selected shape, move their bounding box co-ordinates by the changes in x and y
                for (int i = 0; i < selectedList.Count(); i++)
                {
                    Shape p = (Shape)((list.getShape(selectedList.ElementAt(i).index)).DeepCopy());
                    p.BBoxMax.X = p.BBoxMax.X + changeInX;
                    p.BBoxMax.Y = p.BBoxMax.Y + changeInY;
                    p.BBoxMin.X = p.BBoxMin.X + changeInX;
                    p.BBoxMin.Y = p.BBoxMin.Y + changeInY;
                    p.draw();
                }
            }
        }

        //Resize shape when corners are selected
        public bool resize(ShapeList list, float mX, float mY)
        {
            bool resize = false;
            if (selectedList.Count > 0 && MoveList.size() < 1)
            {
                selectedList.Sort((ref1, ref2) => ref1.index.CompareTo(ref2.index));
                for (int i = 0; i < selectedList.Count(); i++) //Loop selected items
                {
                    Shape temp = list.getShape(selectedList.ElementAt(i).index);
                    if (mX > temp.BBoxMax.X - 3 && mX < temp.BBoxMax.X + 3 && mY > temp.BBoxMax.Y - 3 && mY < temp.BBoxMax.Y + 3) //Selected the top BBox
                    {
                        min = false;
                        index = selectedList.ElementAt(i).index;
                        resize = true;
                        temp.BBoxMax.X = mX;
                        temp.BBoxMax.Y = mY;
                        MoveList.addShape(temp.DeepCopy());
                        MoveList.addShape(temp);
                    }
                    else if (mX > temp.BBoxMin.X - 3 && mX < temp.BBoxMin.X + 3 && mY > temp.BBoxMin.Y - 3 && mY < temp.BBoxMin.Y + 3) //Selected the bottom BBox
                    {
                        resize = true;
                        min = true;
                        index = selectedList.ElementAt(i).index;
                        temp.BBoxMin.X = mX;
                        temp.BBoxMin.Y = mY;
                        MoveList.addShape(temp.DeepCopy());
                        MoveList.addShape(temp);
                    }
                }
            }
            if (MoveList.size() > 0) //Continue to Resize while the list has a item
            {
                resize = true;
                Shape o = MoveList.getLastShape();
                if (min == false)
                {
                    o.BBoxMax.X = mX;
                    o.BBoxMax.Y = mY;
                }
                else
                {
                    o.BBoxMin.X = mX;
                    o.BBoxMin.Y = mY;
                }
                o.draw();
            }
            return resize;
        }

        //resize finished (called when mouse button is unpressed) to stop resizing
        public void resizeFinished(ShapeList list)
        {
            Shape original = MoveList.getFirstShape();
            Shape resized = list.getShape(index);
            //Add Momento states
            if(original.BBoxMax != resized.BBoxMax)
                History.Instance.history.action(new ResizeShapeMemento(resized, resized.BBoxMax.X - original.BBoxMax.X, resized.BBoxMax.Y - original.BBoxMax.Y, true));
            if (original.BBoxMin != resized.BBoxMin)
                History.Instance.history.action(new ResizeShapeMemento(resized, (resized.BBoxMin.X - original.BBoxMin.X), (resized.BBoxMin.Y - original.BBoxMin.Y), false));
            MoveList = new ShapeList(); //Reset resize list
            index = 0;
        }

        //movement finished (called when mouse button is unpressed) to stop moving and save
        public void saveMovement(ShapeList list, float changeInX, float changeInY)
        {
            if (selectedList.Count > 0)
            {
                selectedList.Sort((ref1, ref2) => ref1.index.CompareTo(ref2.index));
                for (int i = 0; i < selectedList.Count(); i++)
                {
                    Shape p = (Shape)((list.getShape(selectedList.ElementAt(i).index))); //Update shapes
                    p.BBoxMax.X = p.BBoxMax.X + changeInX;
                    p.BBoxMax.Y = p.BBoxMax.Y + changeInY;
                    p.BBoxMin.X = p.BBoxMin.X + changeInX;
                    p.BBoxMin.Y = p.BBoxMin.Y + changeInY;
                    History.Instance.history.action(new MoveShapeMemento(p, changeInX, changeInY)); //Add State Change Momento
                }
            }
        }

        //Determine whether a line is close enough to mouse to select
        public int selectLine(Point pointA, Point pointB)
        {
            float p1x = pointA.X, p2x = pointB.X, p1y = pointA.Y, p2y = pointB.Y;
            double l = Math.Sqrt((p2x - p1x) * (p2x - p1x) + (p2y - p1y) * (p2y - p1y));
            double d1 = Math.Sqrt((p1x - mouseX) * (p1x - mouseX) + (p1y - mouseY) * (p1y - mouseY));
            double d2 = Math.Sqrt((p2x - mouseX) * (p2x - mouseX) + (p2y - mouseY) * (p2y - mouseY));
            if ((d1 + d2) - l <= 0.5 || l == d1 + d2)
            {
                return 1;
            }
            return 0;
        }

        //Copy the current selection to clipboard list
        public void copy(float mouseXPos, float mouseYPos, ShapeList list)
        {
            clipboardList = new ShapeList();
            if (selectedList.Count > 0)
            {
                selectedList.Sort((ref1, ref2) => ref1.index.CompareTo(ref2.index));
                for (int i = 0; i < selectedList.Count(); i++)
                {
                    Shape p = (Shape)((list.getShape(selectedList.ElementAt(i).index)).DeepCopy());
                    p.colour.setColour(selectedList.ElementAt(i).actualR, selectedList.ElementAt(i).actualG, selectedList.ElementAt(i).actualB);
                    clipboardList.addShape(p); //Add to clipboard list
                }

                if (mouseXPos + (-1 * Grid.Instance.panX) > 0 && mouseYPos + (-1 * Grid.Instance.panY) > 0) //Use mouse position as reference
                {
                    cPosition = new Point(mouseXPos, mouseYPos);
                }
                else
                {
                    cPosition = new Point(clipboardList.getLastShape().BBoxMax.X, clipboardList.getLastShape().BBoxMax.Y); //Menu item therefore no mouse postion
                }
            }
        }

        //Cut the current selection to clipboard list and remove the items from main list
        public void cut(float mouseXPos, float mouseYPos, ShapeList list)
        {
            clipboardList = new ShapeList();
            if (selectedList.Count > 0)
            {
                selectedList.Sort((ref1, ref2) => ref1.index.CompareTo(ref2.index));
                for (int i = 0; i < selectedList.Count(); i++)
                {
                    Shape p = (Shape)((list.getShape(selectedList.ElementAt(i).index)).DeepCopy());
                    p.colour.setColour(selectedList.ElementAt(i).actualR, selectedList.ElementAt(i).actualG, selectedList.ElementAt(i).actualB);
                    clipboardList.addShape(p); //Add to clipboard list
                }

                if (mouseXPos + (-1 * Grid.Instance.panX) > 0 && mouseYPos + (-1 * Grid.Instance.panY) > 0) //Use mouse position as reference
                {
                    cPosition = new Point(mouseXPos, mouseYPos);
                }
                else
                {
                    cPosition = new Point(clipboardList.getLastShape().BBoxMax.X, clipboardList.getLastShape().BBoxMax.Y); //Menu item therefore no mouse postion
                }
                delete(list); //delete original elements
            }
        }

        //paste a copy of the clipboard list to mouse position
        public void paste(float mouseXPos, float mouseYPos, ShapeList list)
        {
            float movementX, movementY;
            if (mouseXPos + (-1 * Grid.Instance.panX) > 0 && mouseYPos+(-1 * Grid.Instance.panY) > 0 && cPosition != null)
            {
                movementX = mouseXPos - cPosition.X; //Calculate movement from the reference point
                movementY = mouseYPos - cPosition.Y;
            }
            else
            {
                movementX = 10; //Menu paste, therefore no mouse position
                movementY = 10; 
            }
            if (clipboardList.size() > 0)
            {
                for (int i = 0; i < clipboardList.size(); i++) //Loop clipboard list
                {
                    Shape p = clipboardList.getShape(i).DeepCopy();
                    p.BBoxMax.X = p.BBoxMax.X + movementX;
                    p.BBoxMax.Y = p.BBoxMax.Y + movementY;
                    p.BBoxMin.X = p.BBoxMin.X + movementX;
                    p.BBoxMin.Y = p.BBoxMin.Y + movementY;
                    list.addShape(p); //Add the new shape
                    History.Instance.history.action(new AddShapeMemento()); //Add Momento state change action 
                }
            }
            deselectAll(list); //Unselect all
        }

        //Determine whether the mouse lies within a shape's bounding box
        public int selectBBox(Point pointA, Point pointB)
        {
            float p1x = pointA.X, p2x = pointB.X, p1y = pointA.Y, p2y = pointB.Y;
            if (mouseX > p1x && mouseX < p2x && mouseY > p1y && mouseY < p2y)
            {
                return 1;
            }
            return 0;
        }

        //Offset selected shapes to the left by amount specified
        public int offsetLeft(float distance, ShapeList list)
        {
            //If there are selected shapes
            if (selectedList.Count() > 0)
            {
                if (list.getShape(selectedList.ElementAt(0).index).BBoxMin.Y > list.getShape(selectedList.ElementAt(0).index).BBoxMax.Y)
                    distance *= -1;
                Shape newShape;
                Colour col;
                ILine style;
                int shapesInList = selectedList.Count();
                bool imageOrText = false;
                for (int i = 0; i < shapesInList; i++)
                {
                    if (list.getShape(selectedList.ElementAt(i).index).ToString().Equals("Image") || list.getShape(selectedList.ElementAt(i).index).ToString().Equals("Text"))
                    {
                        imageOrText = true;
                    }
                }
                if (imageOrText == true)
                {
                    return 0;
                }
                else
                {
                    //Offset each shape in list
                    for (int i = 0; i < shapesInList; i++)
                    {
                        //Get the points describing the shape
                        Point min = new Point(list.getShape(selectedList.ElementAt(i).index).BBoxMin.X, list.getShape(selectedList.ElementAt(i).index).BBoxMin.Y);
                        Point max = new Point(list.getShape(selectedList.ElementAt(i).index).BBoxMax.X, list.getShape(selectedList.ElementAt(i).index).BBoxMax.Y);
                        //Determine what the shape is
                        String shapeType = list.getShape(selectedList.ElementAt(i).index).ToString();
                        //If the shape is a line, offset left along perpendicular line
                        if (shapeType.Equals("Line"))
                        {
                            float[] vec_line = { max.X - min.X, max.Y - min.Y, 0 };
                            float[] vec_perp = { vec_line[1], (-1) * vec_line[0], 0 };
                            double vec_perp_mag = Math.Sqrt(vec_perp[0] * vec_perp[0] + vec_perp[1] * vec_perp[1]);
                            vec_perp[0] = vec_perp[0] / (float)vec_perp_mag;
                            vec_perp[1] = vec_perp[1] / (float)vec_perp_mag;
                            min.X = list.getShape(selectedList.ElementAt(i).index).BBoxMin.X - distance * vec_perp[0];
                            min.Y = list.getShape(selectedList.ElementAt(i).index).BBoxMin.Y - distance * vec_perp[1];
                            max.X = list.getShape(selectedList.ElementAt(i).index).BBoxMax.X - distance * vec_perp[0];
                            max.Y = list.getShape(selectedList.ElementAt(i).index).BBoxMax.Y - distance * vec_perp[1];
                        }
                        else //If the shape is not a line simply offset left along x-axis
                        {
                            min.X = list.getShape(selectedList.ElementAt(i).index).BBoxMin.X - distance;
                            max.X = list.getShape(selectedList.ElementAt(i).index).BBoxMax.X - distance;
                        }
                        //Create new offset shape
                        switch (shapeType)
                        {
                            case "Chair_side":
                                newShape = new Chair_side(min.X, min.Y, max.X, max.Y);
                                break;
                            case "Chair_top":
                                newShape = new Chair_top(min.X, min.Y, max.X, max.Y);
                                break;
                            case "Circle":
                                newShape = new Circle(min.X, min.Y, max.X, max.Y);
                                break;
                            case "Curve":
                                newShape = new Curve(min.X, min.Y, max.X, max.Y);
                                break;
                            case "Door_side":
                                newShape = new Door_side(min.X, min.Y, max.X, max.Y);
                                break;
                            case "Door_top":
                                newShape = new Door_top(min.X, min.Y, max.X, max.Y);
                                break;
                            case "ExteriorWall":
                                newShape = new ExteriorWall(min.X, min.Y, max.X, max.Y);
                                break;
                            case "InteriorWall":
                                newShape = new InteriorWall(min.X, min.Y, max.X, max.Y);
                                break;
                            case "Line":
                                newShape = new Line(min.X, min.Y, max.X, max.Y);
                                break;
                            case "Square":
                                newShape = new Square(min.X, min.Y, max.X, max.Y);
                                break;
                            case "Stairs_side":
                                newShape = new Stairs_side(min.X, min.Y, max.X, max.Y);
                                break;
                            case "Stairs_top":
                                newShape = new Stairs_top(min.X, min.Y, max.X, max.Y);
                                break;
                            case "Window_side":
                                newShape = new Window_side(min.X, min.Y, max.X, max.Y);
                                break;
                            case "Window_top":
                                newShape = new Window_top(min.X, min.Y, max.X, max.Y);
                                break;
                            default:
                                newShape = null;
                                break;
                        }
                        if (newShape != null)
                        {
                            col = new Colour(selectedList.ElementAt(i).actualR, selectedList.ElementAt(i).actualG, selectedList.ElementAt(i).actualB);
                            style = list.getShape(selectedList.ElementAt(i).index).LineType;
                            newShape.colour = col;
                            newShape.DrawMode = new OpenGLDraw();
                            newShape.LineType = style;
                            //Add shape to main shape list
                            list.addShape((Shape)newShape);
                            History.Instance.history.action(new AddShapeMemento());
                        }
                    }
                    //Deselect selected shapes
                    deselectAll(list);
                    //Select new offset shapes, so they can be offset again if required
                    for (int i = list.size() - shapesInList; i < list.size(); i++)
                    {
                        addShape(list.getShape(i), i);
                    }
                    return 1;
                }
            }
            else
            {
                return 1;
            }
        }

        //Offset selected shapes to the left by amount specified
        public int offsetRight(float distance, ShapeList list)
        {
            //If there are selected shapes
            if (selectedList.Count() > 0)
            {
                if (list.getShape(selectedList.ElementAt(0).index).BBoxMin.Y > list.getShape(selectedList.ElementAt(0).index).BBoxMax.Y)
                    distance *= -1;
                Shape newShape;
                Colour col;
                ILine style;
                int shapesInList = selectedList.Count();
                bool imageOrText = false;
                for (int i = 0; i < shapesInList; i++)
                {
                    if (list.getShape(selectedList.ElementAt(i).index).ToString().Equals("Image") || list.getShape(selectedList.ElementAt(i).index).ToString().Equals("Text"))
                    {
                        imageOrText = true;
                    }
                }
                if (imageOrText == true)
                {
                    return 0;
                }
                else
                {
                    //Offset each shape in list
                    for (int i = 0; i < shapesInList; i++)
                    {
                        //Get the points describing the shape
                        Point min = new Point(list.getShape(selectedList.ElementAt(i).index).BBoxMin.X, list.getShape(selectedList.ElementAt(i).index).BBoxMin.Y);
                        Point max = new Point(list.getShape(selectedList.ElementAt(i).index).BBoxMax.X, list.getShape(selectedList.ElementAt(i).index).BBoxMax.Y);
                        //Determine what the shape is
                        String shapeType = list.getShape(selectedList.ElementAt(i).index).ToString();
                        //If the shape is a line, offset left along perpendicular line
                        if (shapeType.Equals("Line"))
                        {
                            float[] vec_line = { max.X - min.X, max.Y - min.Y, 0 };
                            float[] vec_perp = { vec_line[1], (-1) * vec_line[0], 0 };
                            double vec_perp_mag = Math.Sqrt(vec_perp[0] * vec_perp[0] + vec_perp[1] * vec_perp[1]);
                            vec_perp[0] = vec_perp[0] / (float)vec_perp_mag;
                            vec_perp[1] = vec_perp[1] / (float)vec_perp_mag;
                            min.X = list.getShape(selectedList.ElementAt(i).index).BBoxMin.X + distance * vec_perp[0];
                            min.Y = list.getShape(selectedList.ElementAt(i).index).BBoxMin.Y + distance * vec_perp[1];
                            max.X = list.getShape(selectedList.ElementAt(i).index).BBoxMax.X + distance * vec_perp[0];
                            max.Y = list.getShape(selectedList.ElementAt(i).index).BBoxMax.Y + distance * vec_perp[1];
                        }
                        else //If the shape is not a line simply offset right along x-axis
                        {
                            min.X = list.getShape(selectedList.ElementAt(i).index).BBoxMin.X + distance;
                            max.X = list.getShape(selectedList.ElementAt(i).index).BBoxMax.X + distance;
                        }
                        //Create new offset shape
                        switch (shapeType)
                        {
                            case "Chair_side":
                                newShape = new Chair_side(min.X, min.Y, max.X, max.Y);
                                break;
                            case "Chair_top":
                                newShape = new Chair_top(min.X, min.Y, max.X, max.Y);
                                break;
                            case "Circle":
                                newShape = new Circle(min.X, min.Y, max.X, max.Y);
                                break;
                            case "Curve":
                                newShape = new Curve(min.X, min.Y, max.X, max.Y);
                                break;
                            case "Door_side":
                                newShape = new Door_side(min.X, min.Y, max.X, max.Y);
                                break;
                            case "Door_top":
                                newShape = new Door_top(min.X, min.Y, max.X, max.Y);
                                break;
                            case "ExteriorWall":
                                newShape = new ExteriorWall(min.X, min.Y, max.X, max.Y);
                                break;
                            case "InteriorWall":
                                newShape = new InteriorWall(min.X, min.Y, max.X, max.Y);
                                break;
                            case "Line":
                                newShape = new Line(min.X, min.Y, max.X, max.Y);
                                break;
                            case "Square":
                                newShape = new Square(min.X, min.Y, max.X, max.Y);
                                break;
                            case "Stairs_side":
                                newShape = new Stairs_side(min.X, min.Y, max.X, max.Y);
                                break;
                            case "Stairs_top":
                                newShape = new Stairs_top(min.X, min.Y, max.X, max.Y);
                                break;
                            case "Window_side":
                                newShape = new Window_side(min.X, min.Y, max.X, max.Y);
                                break;
                            case "Window_top":
                                newShape = new Window_top(min.X, min.Y, max.X, max.Y);
                                break;
                            default:
                                newShape = null;
                                break;
                        }
                        if (newShape != null)
                        {
                            col = new Colour(selectedList.ElementAt(i).actualR, selectedList.ElementAt(i).actualG, selectedList.ElementAt(i).actualB);
                            style = list.getShape(selectedList.ElementAt(i).index).LineType;
                            newShape.colour = col;
                            newShape.DrawMode = new OpenGLDraw();
                            newShape.LineType = style;
                            //Add shape to main shape list
                            list.addShape((Shape)newShape);
                        }
                        else
                        {
                            shapesInList--;
                        }
                    }
                    //Deselect selected shapes
                    deselectAll(list);
                    //Select new offset shapes, so they can be offset again if required
                    for (int i = list.size() - shapesInList; i < list.size(); i++)
                    {
                        addShape(list.getShape(i), i);
                    }
                    return 1;
                }
            }
            else
            {
                return 1;
            }
        }

        //If a shape in main shape list is selected, return it
        private selected findInSelectedList(int index)
        {
            selected temp = new selected();
            for (int i = 0; i < selectedList.Count(); i++)
            {
                if (selectedList.ElementAt(i).index == index)
                    temp = selectedList.ElementAt(i);
            }
            return temp;
        }

        //Add shape to selected list
        private void addShape(Shape s, int i)
        {
            selected newSelection = new selected();
            newSelection.index = i;
            newSelection.actualR = s.colour.R;
            newSelection.actualG = s.colour.G;
            newSelection.actualB = s.colour.B;
            selectedList.Add(newSelection);
            s.colour.setColour(255, 0, 0);
        }
    }
}
