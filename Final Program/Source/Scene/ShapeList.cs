﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpGLWPFApplication1
{
        /* A class for creating ShapeList objects to store a list of shapes. */
        public class ShapeList
        {
            //Linked List Shape Storage
            private LinkedList<Shape> list;

            /* Default Constructor */ 
            public ShapeList()
            {
                list = new LinkedList<Shape>();
            }

            /*  Load a texture into the program.     *
             *  e - The Shape Object to add to list  */
            public void addShape(Shape e)
            {
                list.AddLast(e);
            }

            /*  Load a texture into the program.     *
             *  e - The Shape Object to add to list  */
            public Shape getShape(int index)
            {
                return list.ElementAt(index);
            }

            /* Get shape from front of list  */ 
            public Shape getFirstShape()
            {
                return list.First();
            }

            /* Get shape from back of list */ 
            public Shape getLastShape()
            {
                return list.Last();
            }

            /* Get list length */
            public int size()
            {
                return list.Count();
            }

            /* Loop through all shapes and draw to OpenGl */
            public void drawShapes()
            {
                // Loop through the linked list with the foreach-loop and draw.
                foreach (var item in list)
                {
                    item.DrawMode = new OpenGLDraw();
                    item.draw();
                }
            }

            /* Loop through all shapes and draw to PDFSharp */
            public void drawPDFShapes()
            {
                // Loop through the linked list with the foreach-loop and draw.
                foreach (var item in list)
                {
                    item.DrawMode = new PDFDraw();
                    item.draw();
                }
            }

            /* Delete shape from list that matches given shape */
            public bool deleteShape(Shape e)
            {
                if (list.Contains(e))
                {
                    list.Remove(e);
                    return true;
                }
                return false;
            }

            /* Delete shape at given index in list */
            public void deleteShape(int index)
            {
                list.Remove(list.ElementAt(index));
            }

            /* Delete last element in list */
            public void removeLast()
            {
                list.RemoveLast();
            }

            /* Reset list */
            public void clearList()
            {
                list.Clear();
            }
        }
}
